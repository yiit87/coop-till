﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointSystem : MonoBehaviour {
    public static PointSystem Instance; 

    public int AddPointForVitalAndNonVital = 10;
    public int RemovePointFromVital = -10;
    public int AddPointForAgeButtonPressed = 5;
    public int RemovePointForAgeButtonNOTPressed = -5;

    public List<int> RemovePointFor3PeopleInQueue = new List<int>();

    public int RemovePointVitalCustomerMassiveImpact = -30;
    public int RemovePointNonVitalWrongAnswer = -10;



    private void Awake()
    {
        Instance = this;
    }
}
