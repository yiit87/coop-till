﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FailSafeCode : MonoBehaviour {

    public float countDownTimer;
    public float countDown;
    float defaultTimer;

	// Use this for initialization
	void Start ()
    {
        countDown = defaultTimer = CM_Handler.Instance.customerSpawningTimes[0] + countDownTimer;
        
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (!CM_Handler.Instance.GamePause)
        {
            if (CM_Handler.Instance.waitingQueuePositionList.Count < CM_Handler.Instance.targetLocations.Count)
            {
                countDown -= 1f * Time.deltaTime;

                if (countDown <= 0)
                {
                    CM_Handler.Instance.CallTheCustomer(CM_Handler.Instance.NonVitalCustomers);
                    countDownTimer = defaultTimer;
                    Debug.Log("Emergecy is active!");
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        countDown = defaultTimer;
    }
}
