﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TotalCustomerCount : MonoBehaviour {

    private void OnTriggerExit(Collider other)
    {
        CM_Handler.Instance.TotalCustomerSpawned++;
    }
}
