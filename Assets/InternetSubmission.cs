﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class InternetSubmission : MonoBehaviour
{
    public static InternetSubmission Instance;

    public bool idExists = false;
    public bool transmitting;
    private string domain = "http://clicksandlinks.com/coop/ygt/";
    //private string key = "123456"; // Edit this value and make sure it's the same as the one stored on the server
    private string trimmedPost;
    private string storeName;
    private int nameLength;

    public string GetTheData;

    void Awake()
    {
        Instance = this;
        DontDestroyOnLoad(this);
    }
    public bool HasInternet()
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
            return false;
        else
            return true;
    }
    IEnumerator Login(string id)
    {
        if (transmitting)
        {
            print("Already transmitting...");
            yield break;
        }

        transmitting = true;

        string extension = "login.php?";
        string url = domain + extension + "hub=" + WWW.EscapeURL(id);
        WWW post = new WWW(url);
        yield return post;

     

        Debug.Log("post text  = " + post.text);
        Debug.Log("URL = " + url);
        Debug.Log("Post text = " + post.text);
        Debug.Log("Post error = " + post.error);
        Debug.Log("post bytes length = " + post.bytes.Length);
        Debug.Log("post Response header = " + post.responseHeaders.ToString());

        trimmedPost = post.text.Substring(0, 3);
        nameLength = post.text.Length - 3;
        storeName = post.text.Substring(3, nameLength);

        switch (trimmedPost)
        {
            case "OK^":
                print("<b>Login success</b>");
                if (Login1.Instance)
                    Login1.Instance.WasSuccessful(true);

                GetTheData = post.text.Split('^')[1];
                
                JsonUtility.FromJsonOverwrite(GetTheData,DontDestroy.Instance);
                

                idExists = true;
                storeName = post.text.Substring(3, nameLength);
                GlobalData.getInstance().storeName = storeName;
                break;
            case "BAD":
                print("<b>Login failed</b>");
                if (Login1.Instance)
                    Login1.Instance.WasSuccessful(false);

                idExists = false;
                break;
            default:
                print("<b>Login unknown</b>");
                if (Login1.Instance)
                    Login1.Instance.WasSuccessful(false);

                idExists = false;
                break;
        }

        yield return new WaitForEndOfFrame();
        transmitting = false;
        yield break;
    }
}

public class GlobalData
{
    static GlobalData instance;

    public string currentEmployee { get; private set; }

    public string storeName { get; set; }

    public string endText { get; set; }

    public DateTime SessionStartTime { get; private set; }

    public bool isMuted;

    static public GlobalData getInstance()
    {
        if (instance == null)
        {
            instance = new GlobalData();
        }
        return instance;
    }

    public void StartTest(string EmployeeNumber)
    {
        currentEmployee = EmployeeNumber;
        SessionStartTime = DateTime.UtcNow;
    }
}
