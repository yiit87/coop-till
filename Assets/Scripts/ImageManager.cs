﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ImageManager : MonoBehaviour {

    [Header("Parent object for images")]
    public GameObject imageParent;
    private List<GameObject> images = new List<GameObject>();    


    int counter = 0;

	void Start ()
    {
        for (int i = 0; i < imageParent.transform.childCount; i++)
        {
            images.Add(imageParent.transform.GetChild(i).gameObject);

            if (i !=0)
            {
                imageParent.transform.GetChild(i).gameObject.SetActive(false);
            }
        }
	}
	
    public void NextImage()
    {
        if (counter < imageParent.transform.childCount-1)
        {
            counter++;
        }
    }

    public void PrevImage()
    {
        if (counter > 0)
        {
            counter--;
        }
    }
	// Update is called once per frame
	void Update ()
    {
        switch (counter)
        {
            case 0:
                ImageOnOrOff(0);
                break;

            case 1:
                ImageOnOrOff(1);
                break;

            case 2:
                ImageOnOrOff(2);
                break;

            case 3:
                ImageOnOrOff(3);
                break;

            case 4:
                ImageOnOrOff(4);
                break;

            case 5:
                ImageOnOrOff(5);
                break;

            case 6:
                ImageOnOrOff(6);
                break;
        }
	}

    public void ImageOnOrOff(int number)
    {
        for (int i = 0; i < imageParent.transform.childCount; i++)
        {
            if (i == number)
            {
                imageParent.transform.GetChild(i).gameObject.SetActive(true);
            }

            else
            {
                imageParent.transform.GetChild(i).gameObject.SetActive(false);
            }

        }
    }

    public void ReturnToMainMenu()
    {
        int counter = DontDestroy.Instance.imageViewTime;//PlayerPrefs.GetInt("VideoSceneCounter");

        counter++;

        DontDestroy.Instance.imageViewTime = counter;

        DontDestroy.Instance.Click();
        SceneManager.LoadScene("MainMenu");
    }
}
