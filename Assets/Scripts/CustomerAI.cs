﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CustomerAI : MonoBehaviour {

    NavMeshAgent agent;
    Customer1 script;
    bool onlyOnce = true;
    int layerSortingNo;
    public float acceleration = 800f;
    public float deceleration = 100f;

    public float AngularAcce = 800f;
    public float AngularDece = 100f;

    //   public float speed = 100;

    // Use this for initialization
    void Start ()
    {
        // These now initialised in CustomerTypes
        script = GetComponent<Customer1>();

        if (gameObject.name == "Support")
        {
            script.state = State.Spawned;
            script.AddAgent();
        }

        gameObject.transform.localScale = Vector3.one * 8;
        gameObject.GetComponent<BoxCollider>().size = new Vector3(.5f,.5f,.5f);
        gameObject.GetComponent<BoxCollider>().center = new Vector3(0,2,.7f);
    }

    // Update is called once per frame
    void Update()
    {
        if (script.state == State.Pool)
        {
            // Nothing to do until we've spawned
            return;
        }

        NavMeshAgent agent = script.agent;

        if (CM_Handler.Instance.GamePause)
        {
            if (agent.isOnNavMesh)
            {
                agent.isStopped = true;
            }
            agent.GetComponentInChildren<Animator>().SetBool("isWalking", false);
        }
        else
        {
            if (agent.isOnNavMesh)
            {
                agent.isStopped = false;
            }


            if (agent.velocity == Vector3.zero)
            {
                agent.GetComponentInChildren<Animator>().SetBool("isWalking", false);
            }
            else
            {
                agent.GetComponentInChildren<Animator>().SetBool("isWalking", true);
            }
        }
     

        if (GetComponent<Customer1>().customerType != "Support")
        {
            if (CM_Handler.Instance.waitingQueuePositionList.Count != 0)
            {
                for (int i = 0; i < CM_Handler.Instance.waitingQueuePositionList.Count; i++)
                {
                    layerSortingNo = i + 7;

                    // JY NOTE - have seen destroyed GameObject referenced here
                    GameObject go = CM_Handler.Instance.waitingQueuePositionList[i];
                    string sortingLayerName;
                    if (go == null)
                    {
                        sortingLayerName = "";
                        Debug.Log("A destroyed GameObject is on waiting queue");
                    }
                    else
                    {
                        SpriteRenderer sr = CM_Handler.Instance.waitingQueuePositionList[i].GetComponentInChildren<SpriteRenderer>();
                        sortingLayerName = sr.sortingLayerName;
                    }

                    if (go != null &&
                        sortingLayerName != "7" &&
                        sortingLayerName != "5" &&
                        sortingLayerName != "4" &&
                        sortingLayerName != "3" &&
                        CM_Handler.Instance.left == null && 
                        CM_Handler.Instance.right == null)
                    {
                        CM_Handler.Instance.waitingQueuePositionList[i].GetComponentInChildren<SpriteRenderer>().sortingLayerName = layerSortingNo.ToString();
                    }
                }
            }
        }
        else if (GetComponent<Customer1>().customerType == "Support")
        {
            GetComponentInChildren<SpriteRenderer>().sortingLayerName = "1";
        }

        agent.transform.forward = -Vector3.up;
       
        if (agent.isOnNavMesh && onlyOnce)
        {
            if (gameObject.GetComponent<Customer1>().customerName != "Support")
            {
                //Debug.Log("ONCE: (not support) count " + CM_Handler.Instance.targetLocations.Count + ", " + name);
                StartCoroutine(MoveTo(agent, CM_Handler.Instance.targetLocations[CM_Handler.Instance.targetLocations.Count - 1].transform.position));
                onlyOnce = false;
            }
            else
            {
                //Debug.Log("ONCE: (support) zero " + name);
                StartCoroutine(MoveTo(agent, CM_Handler.Instance.supportTargetLocation[0].transform.position));
                onlyOnce = false;
            }
        }

        if (CM_Handler.Instance.GameTimeInSeconds < 0)
        {
            agent.speed = 0;
        }

        if (gameObject.GetComponent<NavMeshAgent>())
        {
            agent.acceleration = (agent.remainingDistance < .5f) ? deceleration : acceleration;
            agent.angularSpeed = (agent.remainingDistance < .5f) ? AngularDece : AngularAcce;
        } 
    }
    IEnumerator WaitABitBeforeSettingLayerOrder(int i)
    {
        yield return new WaitForSeconds(.5f);

        if (CM_Handler.Instance.right != null)
        {
            CM_Handler.Instance.waitingQueuePositionList[i].GetComponentInChildren<SpriteRenderer>().sortingLayerName = "6";
        }
        if (CM_Handler.Instance.left != null)
        {
            CM_Handler.Instance.waitingQueuePositionList[i].GetComponentInChildren<SpriteRenderer>().sortingLayerName = "5";
        }
    }
    IEnumerator MoveTo(NavMeshAgent agent, Vector3 targetLocation)
    {
        yield return new WaitForSeconds(1);
        agent.destination = targetLocation; 
    }
}
