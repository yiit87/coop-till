﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultScreen : MonoBehaviour {

    [Header("Result UI")]
    public GameObject ResultScreenUI;

    [Header("Slider For Happyness Result")]
    public GameObject SliderForResultScreen;

    [Header("Advices For Player")]
    public List<string> Advices = new List<string>();

    [Header("Small Panel in the Result Screen")]
    public GameObject resultPanelColorobject;

  /*  [Header("Result Comment point slots Example: 0 to 15, 15 to 50")]
    public float WorstResultComment;
    public float MiddleMinusResultComment;
    public float MiddlePlusResultComment;
    public float GoodResultComment;*/

    public GameObject AdviceText;

    public GameObject ScoreValue;

	// Use this for initialization
	void Start ()
    {
        if (ResultScreenUI.activeInHierarchy)
        {
            ResultScreenUI.SetActive(false);
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (CM_Handler.Instance.GameTimeInSeconds <= 0 )
        {
            if (CM_Handler.Instance.TotalHappyness >= 75 && CM_Handler.Instance.TotalHappyness <= 100)
            {
                AdviceText.GetComponent<Text>().text = Advices[0];
                resultPanelColorobject.GetComponent<Image>().color = new Color32(100, 170, 140, 255);
            }
            else if (CM_Handler.Instance.TotalHappyness >= 50 && CM_Handler.Instance.TotalHappyness <= 74)
            {
                AdviceText.GetComponent<Text>().text = Advices[1];
                resultPanelColorobject.GetComponent<Image>().color = new Color32(133, 133, 133, 255);
            }
            else if (CM_Handler.Instance.TotalHappyness >= 25 && CM_Handler.Instance.TotalHappyness <= 49)
            {
                AdviceText.GetComponent<Text>().text = Advices[2];
                resultPanelColorobject.GetComponent<Image>().color = new Color32(133, 133, 133, 255);
            }
            else if (CM_Handler.Instance.TotalHappyness >= 0 && CM_Handler.Instance.TotalHappyness <= 24)
            {
                AdviceText.GetComponent<Text>().text = Advices[3];
                resultPanelColorobject.GetComponent<Image>().color = new Color32(180, 30, 60, 255);
            }

            #region Happyness Faces
            if (CM_Handler.Instance.TotalHappyness < 10)
            {
                SliderForResultScreen.transform.GetChild(2).GetComponentInChildren<Image>().sprite = Resources.Load<Sprite>("SadHappyAnim_0001");
            }
            else if (CM_Handler.Instance.TotalHappyness > 10 && CM_Handler.Instance.TotalHappyness < 20)
            {
                SliderForResultScreen.transform.GetChild(2).GetComponentInChildren<Image>().sprite = Resources.Load<Sprite>("SadHappyAnim_0020");
            }
            else if (CM_Handler.Instance.TotalHappyness > 20 && CM_Handler.Instance.TotalHappyness < 40)
            {
                SliderForResultScreen.transform.GetChild(2).GetComponentInChildren<Image>().sprite = Resources.Load<Sprite>("SadHappyAnim_0035");
            }
            else if (CM_Handler.Instance.TotalHappyness > 40 && CM_Handler.Instance.TotalHappyness < 60)
            {
                SliderForResultScreen.transform.GetChild(2).GetComponentInChildren<Image>().sprite = Resources.Load<Sprite>("SadHappyAnim_0050");
            }
            else if (CM_Handler.Instance.TotalHappyness > 60 && CM_Handler.Instance.TotalHappyness < 80)
            {
                SliderForResultScreen.transform.GetChild(2).GetComponentInChildren<Image>().sprite = Resources.Load<Sprite>("SadHappyAnim_0065");
            }
            else if (CM_Handler.Instance.TotalHappyness > 80 && CM_Handler.Instance.TotalHappyness < 90)
            {
                SliderForResultScreen.transform.GetChild(2).GetComponentInChildren<Image>().sprite = Resources.Load<Sprite>("SadHappyAnim_0080");
            }
            else if (CM_Handler.Instance.TotalHappyness > 90)
            {
                SliderForResultScreen.transform.GetChild(2).GetComponentInChildren<Image>().sprite = Resources.Load<Sprite>("SadHappyAnim_0100");
            }
            #endregion  

            if (SliderForResultScreen.activeInHierarchy)
            {
                SliderForResultScreen.GetComponent<Slider>().value = CM_Handler.Instance.TotalHappyness;
                ScoreValue.GetComponent<Text>().text = CM_Handler.Instance.TotalHappyness.ToString();
            }
            ResultScreenUI.SetActive(true);
            CM_Handler.Instance.GamePause = true;
        }
	}
}
