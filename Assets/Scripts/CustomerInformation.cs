﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class CustomerInformation : MonoBehaviour
{
    public static CustomerInformation Instance;


    [Header("Information Panel")]
    public GameObject TextPanel;

    TMP_Text updateText;
    string infoBubble;
    bool stay;//, exit;

    public GameObject AnyButtonPress;

    private void Awake()
    {
        stay = false;
        //exit = false;
        Instance = this;
    }

    // Use this for initialization
    void Start()
    {
        updateText = TextPanel.GetComponent<TMP_Text>();
    }
    private void OnTriggerStay(Collider other)
    {
        stay = true;
    }
    private void OnTriggerExit(Collider other)
    {
        stay = false;
        if (AnyButtonPress != null)
        {
            if (AnyButtonPress.transform == other.transform)
            {
                //Debug.Log("Clear Anybutton press for " + AnyButtonPress.name + " by " + other.name);
                AnyButtonPress = null;
            }
            else
            {
                Debug.Log("Not clearing Anybutton press for " + AnyButtonPress.name + " by " + other.name);
            }
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (CM_Handler.Instance.waitingQueuePositionList.Count != 0)
        {
            if (CM_Handler.Instance.waitingQueuePositionList[0].GetComponent<NavMeshAgent>().remainingDistance > 2f && !stay)
            {
                updateText.transform.parent.gameObject.SetActive(false);
            }
            else
            {
                if (!QuestionManager.Instance.buttonPressed & 
                    stay && 
                    CM_Handler.Instance.waitingQueuePositionList[0].GetComponent<NavMeshAgent>().velocity == Vector3.zero &&
                    CM_Handler.Instance.waitingQueuePositionList[0].GetComponent<NavMeshAgent>().remainingDistance <= 0.0000000001f     // was 0.0000000001f
                    && AnyButtonPress == null)
                {
                    Customer1 script = CM_Handler.Instance.waitingQueuePositionList[0].GetComponent<Customer1>();
                    if (script.state != State.InFirstQueue)
                    {
                        // JY NOTE - When there's a queue at the kiosk, we often hit  this condition. You'd then get to dispatch the customer again (before other changes, it could dispatch the next customer which caused chaos)
                        // We've already dispatched this customer
                        //Debug.Log("INFO BOX ATTEMPT FOR CUSTOMER '" + script.customerName + "' IN WRONG STATE - state " + script.state);
                        updateText.transform.parent.gameObject.SetActive(false);
                    }
                    else
                    {
                        updateText.transform.parent.gameObject.SetActive(true);
                    }
                }
                else
                {
                    //Debug.Log("remaining distance " + CM_Handler.Instance.waitingQueuePositionList[0].GetComponent<NavMeshAgent>().remainingDistance);
                    updateText.transform.parent.gameObject.SetActive(false);
                }
            }
            infoBubble = CM_Handler.Instance.waitingQueuePositionList[0].GetComponent<Customer1>().informationBuble;

            string all = infoBubble;
            updateText.text = all.ToString();
        }
        else
        {
            updateText.transform.parent.gameObject.SetActive(false);
        }
    }

}
