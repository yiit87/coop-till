﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SwipeDirection
{
    None = 0,
    Left =1,
    Right =2,
}
public class SwipeLeftRight : MonoBehaviour {

    private static SwipeLeftRight instance;

    public static SwipeLeftRight Instance
    {
        get
        {
            return instance;
        }
    }

    public SwipeDirection Direction { set; get; }
    private Vector3 touchPosition;
    private float swipeResistanceX = 50.0f;
    //private float swipeResistanceY = 100f;
    
    void Awake()
    {
        instance = this;
    }

	
	// Update is called once per frame
	void Update ()
    {
#if UNITY_STANDALONE || UNITY_EDITOR

        Direction = SwipeDirection.None;

        if (Input.GetMouseButtonDown(0))
        {
            touchPosition = Input.mousePosition;
        }
        if (Input.GetMouseButtonUp(0))
        {
            Vector2 deltaSwipe = touchPosition - Input.mousePosition;

            if (Mathf.Abs(deltaSwipe.x) > swipeResistanceX)
            {
                //
                Direction |= (deltaSwipe.x <0) ? SwipeDirection.Right: SwipeDirection.Left;
            }
        }
#endif
    }

    public bool IsSwiping(SwipeDirection dir)
    {
        if (dir == Direction)
        {
            return true;
        }
        else { return false; }
    }
}
