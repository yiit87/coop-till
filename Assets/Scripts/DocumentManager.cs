﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DocumentManager : MonoBehaviour {

	public void BackToMainMenu()
    {
        int counter = DontDestroy.Instance.documentReadingTime;//PlayerPrefs.GetInt("VideoSceneCounter");

        counter++;

        DontDestroy.Instance.documentReadingTime = counter;

        DontDestroy.Instance.Click();
        SceneManager.LoadScene("MainMenu");
    }
}
