﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddCustomer : MonoBehaviour
{
    public static AddCustomer Instance;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        CustomerType<Customer1> customer1;

        #region Support
        customer1 = new CustomerType<Customer1>("Support", "Support");
        customer1.scriptComponent.Initialize(
           nameOfTheCustomer: "Support",
           customerTypeData: "Support", //Kid,Awkward
           infoBubble: "Support",
           speeD: 5.5f,
           customerNo: 001,
           questions: new string[] { "Support/C", "Support/R", "Support/N" },
           result: new string[] { "Support", "Support", "Support" },
           where: "Kiosk",
           SecondC: false,
           MassiveI: false,
           ARS: false,
           chSprite: Resources.Load<Sprite>("Male colleague (1)"));
        #endregion
        
        #region Vital 7:00
        customer1 = new CustomerType<Customer1>("Builder", "7:00");
        customer1.scriptComponent.Initialize(
           nameOfTheCustomer: "Builder",
           customerTypeData: "Vital", //Kid,Awkward
           infoBubble: "<b>Builder</b> \n\n<align=left>•<indent=3.5em>He looks cheerful</indent>\n•<indent=3.5em>Carrying a newspaper and Red Bull and always buys the same cigarettes</indent>\n•<indent=3.5em>He's a regular and he always likes to have a chat</indent><line-height=0>",
           speeD: 5.5f,
           customerNo: 999,
           questions: new string[]
           {
               "Leave him to it, carry on with what you were doing/R",
               "The store is empty, invite him over to the ASTs to scan his items while you offer to get his usual pack of cigarettes/C",
               "Invite him over to the kiosk and have a chat with him and serve him as you usually would/N"
           },
           result: new string[]
           {
               "He enjoys having a chat with colleagues in the morning; he feels ignored and unwelcomed",
               "You offer to get him his usual pack of cigarettes from the kiosk. He's pleased-  not only does it save him time, it's impressive that you remembered",
               "He's used to this and is neither surprised or disappointed - this is the habit you're both used to. He comes in at the same time on the same days and buys the same things so why should you try doing anything different? Right?"
           },
           where: "AST", 
           SecondC: false,
           MassiveI: false,
           ARS: true,
           chSprite: Resources.Load<Sprite>("GuyPPE-02"));
        #endregion

        #region Vital 7:30
        customer1 = new CustomerType<Customer1>("Nurse", "7:30");
        customer1.scriptComponent.Initialize(
           nameOfTheCustomer: "Nurse*", 
           customerTypeData: "Vital", //Kid,Awkward
           infoBubble: "<b>Nurse</b> \n\n<align=left>•<indent=3.5em>She's wearing her uniform, looks like she's just finished her shift and keeps yawning</indent>\n•<indent=3.5em>Carrying milk, coffee, biscuits, toothpaste and red wine</indent><line-height=0>",
           speeD: 5.5f,
           customerNo: 999,
           questions: new string[]
           {
               "Acknowledge her coming into the store and make sure that you're ready to greet her in the service area/C",
               "She's probably going to be a while, you leave the service area and take some cardboard to the warehouse/R",
               "She picks up a few items. Wait behind the kiosk as she's buying alcohol/N"
           },
           result: new string[]
           {
               "Greeting her made her think you were ready to help her. She comes over to the service area you ask her if she'd like to use the AST and she says that she has wine. You say that this isn't a problem and go with her to the AST to approve the transaction",
               "Having just finished a long shift, being left stood without any help for 2 or 3 minutes was the last thing she needed. She puts down her goods and walks out",
               "She sees you stood behind the kiosk so heads over looking confused with the new layout. She tells you about her day and you offer to show her how to use the ASTs next time to save her time"
           },
           where: "AST",
           SecondC: false,
           MassiveI: true,
           ARS: true,
           chSprite: Resources.Load<Sprite>("Nurse-01"));
        #endregion

        #region Vital 8:30
        customer1 = new CustomerType<Customer1>("Pensioner", "8:30");
        customer1.scriptComponent.Initialize(
           nameOfTheCustomer: "Pensioner*",
           customerTypeData: "Vital",
           infoBubble: "<b>Pensioner</b> \n\n<align=left>•<indent=3.5em>He has a cane to help him walk</indent>\n•<indent=3.5em>Carrying a basket of milk and an electricity top up key</indent>\n•<indent=3.5em>Struggling to get around the store and carry his basket</indent><line-height=0>",
           speeD: 5.5f,
           customerNo: 001,
           questions: new string[]
           {
               "Tell him that he can now use the ASTs and leave him to it/R",
               "Ask him if you can help him with his transaction through the ASTs and take him over/N",
               "You see him struggling so you take him to the kiosk so he can comfortably unload his items on to the belt/C"
           },
            result: new string[]
            {
                "He struggles at the ASTs, as he puts the basket down he drops his stick. He's visibly distressed by this and is confused as to how he can top up his electricity",
                "He initially doesn't want to use the ASTs alone but appreciates the help, however he takes a long time and wants a paypoint service so has to use to kiosk afterwards",
                "He really appreciates the help and feels comfortable to chat and complete his paypoint purchase as he usually does"
            },
           where: "Kiosk",
           SecondC: false,
           MassiveI: true,
           ARS: false,
           chSprite: Resources.Load<Sprite>("OldGuy-02"));
        #endregion

        #region Vital 9:00
        customer1 = new CustomerType<Customer1>("Police Officer", "9:00");
        customer1.scriptComponent.Initialize(
           nameOfTheCustomer: "Police Officer",
           customerTypeData: "Vital",
           infoBubble: "<b>Police Officer</b> \n\n<align=left>•<indent=3.5em>She looks stressed, in a hurry and wants to get in and out quickly</indent>\n•<indent=3.5em>Carrying a couple of bottles of lucozade</indent>\n•<indent=3.5em>The store is fairly busy</indent><line-height=0>",
           speeD: 5.5f,
           customerNo: 001,
           questions: new string[]
           {
               "She looks stressed - you don't want to make this worse so you stand by the kiosk and wait/N",
               "You notice that she looks stressed and in a hurry. Stand behind the kiosk in order to have a conversation with her and try to help cheer her up/R",
               "You greet her pleasantly and ask if she'd like to pay at the ASTs as they're quick and easy to use/C"
           },
            result: new string[]
            {
                "This is what the police officer usually expects. She's had to wait behind one other customer but is able to buy her products like she usually does",
                "You start trying to have a conversation with her. She's clearly stressed and in a rush and isn't interested in waiting behind someone else or having a chat. She is frustrated and mutters something under her breath as she leaves",
                "You reassured her of how quick and easy the ASTs are, she was able to pay for her products quickly and leave without a fuss. She thanks you and says she will use them again in future"
            },
         where: "AST", 
         SecondC: false,
         MassiveI: false,
         ARS: false,
         chSprite: Resources.Load<Sprite>("Police-02"));
        #endregion

        #region Vital 10:00
        customer1 = new CustomerType<Customer1>("Mum with Pram", "10:00");//10 
        customer1.scriptComponent.Initialize(
        nameOfTheCustomer: "Mum with Pram",
        customerTypeData: "Vital",
        infoBubble: "<b>Young mum with pram</b> \n\n<align=left>•<indent=3.5em>She looks tired</indent>\n•<indent=3.5em>Carrying a basket with formula, nappies and some household essentials</indent>\n•<indent=3.5em>She's a regular and has her newborn baby with her</indent><line-height=0>",
        speeD: 5.5f,
        customerNo: 001,
        questions: new string[]
        {
                "Greet her as she comes in,  position yourself by the ASTs/N",
                "The store is pretty quiet so offer her the choice of using ASTs if she wants to or the kiosk. Help by offering to carry the basket to the service area for her and unload the items and pack/C",
                "Stand behind the kiosk and wait for her to unload all of her purchases ready for you to scan/R"
        },
         result: new string[]
         {
                "ASTs are quick and simple enough. However, she's really struggling to balance all of the products and bag them up whilst pushing the pram too",
                "You made her experience much easier. She chooses the AST so she can take things easy and just have a chat with you whilst you scan her products through. She thought that she couldn't get any help if she used this kind of till",
                "This wasn't the most helpful option, you can see that she was struggling to juggle everything. It's important that you don't stay still in the service area when you're there"
         },
         where: "AST", 
         SecondC: false,
         MassiveI: false,
         ARS: false,
         chSprite: Resources.Load<Sprite>("WomanWithPram-01"));

        #endregion

        #region Vital 11:30
        customer1 = new CustomerType<Customer1>("YoungMan", "11:30");
        customer1.scriptComponent.Initialize(
        nameOfTheCustomer: "YoungMan*",
        customerTypeData: "Vital",
        infoBubble: "<b>Young man</b> \n\n<align=left>•<indent=3.5em>He's wearing a tracksuit and cap</indent>\n•<indent=3.5em>Carrying a basket of crisps</indent>\n•<indent=3.5em>He's talking very loudly and swearing on the phone</indent><line-height=0>",
        speeD: 5.5f,
        customerNo: 001,
        questions: new string[]
        {
               "He doesn't look like the kind of person you want to get into a conversation with, just get on with your tasks/R",
               "He's got a basket full of crisps and snacks, he's probably going to use the AST. Carry on with what you were doing in the service area and leave him to it/N",
               "The store is fairly busy and he has a basket full of crisps and snacks. Invite him to use the ASTs and make sure you're nearby if he needs help/C"
        },
         result: new string[]
         {
                "He goes to the AST and has issues because he scans his items too many times. Whilst you responded to the intervention once you noticed, he gets impatient. He also mentioned it quite loudly to his friend on the phone - other customers see this",
                "He sees you nearby and shouts over as he needs help with the till - he has a habit of triggering interventions by not scanning his items properly. Thankfully, you're on hand to help",
                "He heads to the AST after you invited him but he says that he thinks they're rubbish. You show him how to use them and avoid triggering interventions. He thanks you as he leaves"
         },
         where: "AST", 
         SecondC: false,
         MassiveI: true,
         ARS: false,
         chSprite: Resources.Load<Sprite>("GuyWithCap"));

        #endregion

        #region Vital 12:00
        customer1 = new CustomerType<Customer1>("Old Woman", "12:00");
        customer1.scriptComponent.Initialize(
        nameOfTheCustomer: "Old Woman%",
        customerTypeData: "Vital",
        infoBubble: "<b>Older woman</b> \n\n<align=left>•<indent=3.5em>She's dressed in casual clothing</indent>\n•<indent=3.5em>Carrying no items</indent>\n•<indent=3.5em>She has a phone, bank card and wants to buy cigars</indent><line-height=0>",
        speeD: 5.5f,
        customerNo: 999,
        questions: new string[]
        {
                "Let her choose the appropriate till, talk to her if she talks to you/N",
                "You know the store is busy, seeing that she has no products you approach her and ask if you can help and direct her to the Card only ASTs whilst you get her item from the kiosk/C",
                "You know the store is busy, seeing that she has no products you approach her and ask if you can help and direct the customer to the kiosk so she can choose products from there/R"
        },
       result: new string[]
         {
                "She is confused by the new layout, eventually you offer to get her cigars while she goes to the ASTs but this means she is stood around for a while. A queue starts to form",
                "She feels in control of the situation and doesn't have to wait, whilst being pleasantly surprised you go the extra mile to get the item she wants",
                "Because there are other customers at the kiosk she ends up waiting longer than she expected and is frustrated because she doesn't want to wait behind others for a pack of cigars"
         },
         where: "AST", 
         SecondC: true,
         MassiveI: false,
         ARS: true,
         chSprite: Resources.Load<Sprite>("OldGirl-02"));
        #endregion

        #region Vital 12:30
        customer1 = new CustomerType<Customer1>("Wheelchair User", "12:30");
        customer1.scriptComponent.Initialize(
        nameOfTheCustomer: "&Wheelchair User%",//& wheelchair user ?
        customerTypeData: "Vital",
        infoBubble: "<b>Young man</b> \n\n<align=left>•<indent=3.5em>He's a wheelchair user</indent>\n•<indent=3.5em>Carrying a lot of items in a basket and wants to claim his Lotto winnings</indent>\n•<indent=3.5em>Seems stressed when he sees the new layout</indent><line-height=0>",
        speeD: 5.5f,
        customerNo: 001,
        questions: new string[]
        {
               "You let him choose where to go and then react/N",
               "You take his basket and kindly tell him you'll put all of his shopping through. You offer to push his wheelchair over to the ASTs/R",
               "You ask him if he'd like to use the ASTs and if he'd like any further assistance. You check how he'd like to pay and if there is anything else he needs/C"
        },
       result: new string[]
         {
                "You eventually offer help when he approaches the kiosk but this isn't ideal - you can see that the customer was visibly stressed by the new layout and a simple \"Hello, would you like to use this Till?\" would have helped reassure him",
                "On this occasion, he takes offence to the assumption he'd want to be pushed around. Since he also wants to claim a Lotto prize he would like to be served at the kiosk",
                "This pro-active but unassuming attitude helps put him at ease and because he wants to claim a Lotto prize you serve him at the kiosk"
         },
        where: "Kiosk", 
        SecondC: true,
        MassiveI: false,
        ARS: false,
        chSprite: Resources.Load<Sprite>("Wheelchair-02"));
        #endregion

        #region Vital 12:45
        customer1 = new CustomerType<Customer1>("Grandmother and grandchild", "12:45");
        customer1.scriptComponent.Initialize(
        nameOfTheCustomer: "Grandmother and grandchild%",
        customerTypeData: "Vital",
        infoBubble: "<b>Grandmother and grandchild</b> \n\n<align=left>•<indent=3.5em>She's chatting to her grandchild</indent>\n•<indent=3.5em>Carrying two baskets full of fresh produce</indent>\n•<indent=3.5em>They seem like they're in no rush</indent><line-height=0>",
        speeD: 5.5f,
        customerNo: 001,
        questions: new string[]
        {
               "They look like they're in no rush so leave them to it and make sure that you position yourself in the middle of the service area/N",
               "It looks as if they're going to be some time getting everything they need, especially with two baskets full. Carry on tidying up the impulse products/R",
               "Position yourself in the middle of the service area as they approach and direct them towards the ASTs and offer them some help completing their transaction/C"
        },
       result: new string[]
       {
                "You were stood in the service area and directed them to the ASTs but didn't offer any further assistance",
                "You missed their approach to the service area because you were tidying things up and they ended up stood there for a while confused as to where to go",
                "Positioning yourself in the middle of the service area and offering them assistance made a big difference to their experience. The lady was struggling to carry her basket so you take it off her and she appreciates the help. You offer to complete the transaction for her - she accepts and her granddaughter enjoys helping too"
         },
        where: "AST", 
        SecondC: true,
        MassiveI: false,
        ARS: false,
        chSprite: Resources.Load<Sprite>("OldWomanWithGrandkids-01"));
        #endregion

        #region Vital 13:00
        customer1 = new CustomerType<Customer1>("Uni Student", "13:00");
        customer1.scriptComponent.Initialize(
        nameOfTheCustomer: "Uni Student",
        customerTypeData: "Vital",
        infoBubble: "<b>University student</b> \n\n<align=left>•<indent=3.5em>She's on the phone</indent>\n•<indent=3.5em>Carrying snacks and wants to buy some spirits</indent>\n•<indent=3.5em>She is in a hurry</indent><line-height=0>",
        speeD: 5.5f,
        customerNo: 999,
        questions: new string[]
        {
               "She's in a world of her own and looks like she knows what she's doing- leave her to it and complete your gap checks/R",
               "Give her a subtle wave and a smile. Go to the kiosk - she's got her phone in one hand so might struggle with an AST/N",
               "Give her a subtle wave and a smile. Position yourself in the middle of the service area ready to help out/C"
        },
       result: new string[]
       {
                "She's distracted because she's on the phone and the new layout confuses her.  She's unsure whether she can use the ASTs because she wants to buy spirits",
                "As you know she wants to buy a spirit you serve her at the kiosk, however, she could have started scanning her items at the AST if you had offered to retrieve the spirits from the kiosk",
                "Positioning yourself in the middle meant that you could go straight to her and help. You tell her that you can get a bottle of spirits from the kiosk as she scans her other items, dealing with age verification straight away - she was impressed by this and mentioned it to her friend"
         },
       where: "AST",
       SecondC: false,
       MassiveI: false,
       ARS: true,
       chSprite: Resources.Load<Sprite>("GirlWithBackpack-01"));
        #endregion

        #region Vital 15:00
        customer1 = new CustomerType<Customer1>("Older Man", "15:00");
        customer1.scriptComponent.Initialize(
        nameOfTheCustomer: "Older Man",
        customerTypeData: "Vital",
        infoBubble: "<b>Older man</b> \n\n<align=left>•<indent=3.5em>He's dressed in casual clothes</indent>\n•<indent=3.5em>He has a few items and wants to claim a lottery prize</indent>\n•<indent=3.5em>He comes in this store fairly regularly</indent><line-height=0>",
        speeD: 5.5f,
        customerNo: 999,
        questions: new string[]
        {
               "Leave him to it and focus on replenishing produce/R",
               "As he's only got a couple of items so you position yourself in the middle of the service area and direct him to the ASTs/N",
               "As he is claiming a Lotto prize you ask if he would like to come over to the kiosk and complete his transaction straight away/C"
        },
        result: new string[]
         {
                "As you were busy filling up you missed him going over to the service area. He went straight to the kiosk to redeem his lottery winnings.  Leaving him waiting means he gets impatient and he puts down his items and walks out of the store",
                "He follows your guidance and starts scanning his items. He asks you to cash in his  lottery and then to use the winnings to buy more lottery tickets. As you now need to complete another transaction for him at the kiosk a queue is starting to form",
                "He approaches the service area and you ask him how you can help. He tells you he has a couple of items and needs to cash in some scratch cards and a lottery ticket plus use the winnings to buy more"
         },
        where: "Kiosk", 
        SecondC: false,
        MassiveI: false,
        ARS: true,
           chSprite: Resources.Load<Sprite>("OldGuy-03"));
        #endregion

        #region Vital 16:00
        customer1 = new CustomerType<Customer1>("SchoolChild", "16:00");
        customer1.scriptComponent.Initialize(
        nameOfTheCustomer: "SchoolChild%",
        customerTypeData: "Vital",
        infoBubble: "<b>Schoolchild</b> \n\n<align=left>•<indent=3.5em>He is dressed in school uniform</indent>\n•<indent=3.5em>Carrying chocolate and soft drinks</indent>\n•<indent=3.5em>He is playing on his phone and usually uses the ASTs</indent><line-height=0>",
        speeD: 5.5f,
        customerNo: 001,
        questions: new string[]
        {
                "Ask if he'd like to pay by cash or card and direct him to the correct type of AST/C",
                "Tell him you'll serve him through the kiosk as this is where you're used to standing/R",
                "Accompany him to the ASTs and complete his transaction for him/N"
        },
        result: new string[]
         {
                "He uses this kind of technology all the time so pays and leaves quickly without needing any help",
                "He doesn't want to interact and ignores you, this also holds up the area for other customers and his mates laugh at him",
                "He feels a bit patronised as he knows what he's doing and other customers need your help more"
         },
        where: "AST",
        SecondC: true,
        MassiveI: false,
        ARS: false,
        chSprite: Resources.Load<Sprite>("GroupOfKids-01"));
        #endregion

        #region Vital 16:00 2
        customer1 = new CustomerType<Customer1>("Dad With Older Kids", "16:00");
        customer1.scriptComponent.Initialize(
        nameOfTheCustomer: "Dad With Older Kids",
        customerTypeData: "Vital",
        infoBubble: "<b>Dad with older kids</b> \n\n<align=left>•<indent=3.5em>He's just picked them up from school </indent>\n•<indent=3.5em>Carrying pizzas, milk and frozen food in a couple of baskets and would also like to buy a bus pass</indent>\n•<indent=3.5em>The kids are messing about, he has his hands full</indent><line-height=0>",
        speeD: 5.5f,
        customerNo: 999,
        questions: new string[]
        {
               "Offer to take his baskets and invite him to the kiosk and call for support if needed so you can give him your full attention/C",
               "Position yourself in the centre of the service area ready to serve him/N",
               "Tell him to go to the ASTs so that he can put all his items down there. Explain that you'll be over to serve him soon/R"
        },
        result: new string[]
         {
               "He's impressed that you spotted he needed help and went out of your way, this relieves some of the stress and you can also sell him the bus pass",
               "He knows you're on hand should he need you. He's pretty stressed though between carrying all of the shopping and looking after his children - he would have appreciated a bit of help",
               "He feels like you've just told him where to go without offering any help and he still needs his bus pass. To him, it looks like you're just stood there doing nothing and that he isn't really a priority"
         },
        where: "Kiosk", 
        SecondC: false,
        MassiveI: false,
        ARS: true,
           chSprite: Resources.Load<Sprite>("ManWithKids-03"));

        #endregion

        #region Vital 17:00
        customer1 = new CustomerType<Customer1>("Older Worker", "17:00");
        customer1.scriptComponent.Initialize(
        nameOfTheCustomer: "Older Worker%",
        customerTypeData: "Vital",
        infoBubble: "<b>Older worker</b> \n\n<align=left>•<indent=3.5em>He's dressed in a suit and looks tired</indent>\n•<indent=3.5em>Carrying a full basket of shopping</indent>\n•<indent=3.5em>He seems to be in a rush and doesn't like using ASTs</indent><line-height=0>",
        speeD: 5.5f,
        customerNo: 001,
        questions: new string[]
        {
               "Ask him if he'd like to pay by cash or card and direct him to the correct type of AST/R",
               "Go to the kiosk and serve him as he doesn't like ASTs/N",
               "Ask if he'd like to use the ASTs and take him over to the till, staying on hand to help him complete the transaction/C"
        },
        result: new string[]
         {
                "He asks why you can't serve him at the kiosk as you usually would and reluctantly starts to use the ASTs but isn't confident and gets frustrated when he puts an item in the wrong place and decides to queue at the kiosk instead",
                "He is used to being served at the kiosk and you serve him there as he expects",
                "He says he doesn't like ATSs but agrees to try them when you show him how to use them. He then sees how easy and quick they are to use"
         },
        where: "AST", 
        SecondC: true,
        MassiveI: false,
        ARS: false,
        chSprite: Resources.Load<Sprite>("BusinessGuy-03"));
        #endregion

        #region Vital 17:30
        customer1 = new CustomerType<Customer1>("COOP Colleague", "17:30");
        customer1.scriptComponent.Initialize(
        nameOfTheCustomer: "COOP Colleague*",
        customerTypeData: "Vital",
        infoBubble: "<b>Co-op colleague</b> \n\n<align=left>•<indent=3.5em>She's wearing her co-op uniform</indent>\n•<indent=3.5em>Carrying pizza and some beers</indent>\n•<indent=3.5em>You know her well and she likes to have a good chat</indent><line-height=0>",
        speeD: 5.5f,
        customerNo: 001,
        questions: new string[]
        {
               "She always uses the kiosk and you know she will want to chat so serve her there/N",
               "She always uses the kiosk. Ask her if she'd like to use the ASTs so that you can show them how quick and easy they are/C",
               "Seeing as she only has a few items, she will probably use the ASTs. Get on with your other tasks and just make sure you listen out for the intervention for the beers/R"
        },
       result: new string[]
         {
                "She always uses the kiosk - it's her habit. However, if you explained the benefits of using the ASTs, not only would it impress her but she'd definitely be positive about it with other colleagues, friends and family",
                "What a service! Working in a store, she probably has some expectation as to the level of service she should receive. Going out of your way like this and taking the time to really impress her will exceed her expectations",
                "She went to the kiosk, as she always does out of habit. She ended up standing there for a couple of minutes waiting and then moaning to you about the changes, in front of other customers"
         },
        where: "AST",
        SecondC: false,
        MassiveI: true,
        ARS: false,
        chSprite: Resources.Load<Sprite>("Older female colleague"));
        #endregion

        #region Vital 18:00
        customer1 = new CustomerType<Customer1>("Young Pro", "18:00");
        customer1.scriptComponent.Initialize(
        nameOfTheCustomer: "Young Pro%",
        customerTypeData: "Vital",
        infoBubble: "<b>Young professional</b> \n\n<align=left>•<indent=3.5em>He's wearing a suit with his pass round his neck</indent>\n•<indent=3.5em>Carrying a bottle of wine</indent>\n•<indent=3.5em>He's in a rush</indent><line-height=0>",
        speeD: 5.5f,
        customerNo: 999,
        questions: new string[]
        {
               "It's busy in the store and you can see from his expression that he doesn't want to wait in a queue - you ask him how he'll be paying and invite him over to the ASTs/C",
               "He looks like he knows what he's doing and people like him always use self scan tills, you leave him to it and see what help the next customers need/R",
               "You see he has a bottle of wine so you guide him to the kiosk area to be served/N"
        },
       result: new string[]
         {
                "He's happy that you've prioritised getting him out of the store quickly after a long day at work and you were able to authorise his age related sale quickly",
                "He scans his wine and then has to wait for you to come over to approve it, this frustrates him as he just wants to leave and sees other customers come and go as he stands waiting",
                "He is served through the kiosk after waiting for one other customer to finish and you approve the wine sale as you scan. He isn't in and out as quickly as he could be"
         },
        where: "AST",
        SecondC: true,
        MassiveI: false,
        ARS: true,
        chSprite: Resources.Load<Sprite>("BusinessGuy-02"));
        #endregion

        #region Vital 19:15
        customer1 = new CustomerType<Customer1>("Vicar", "19:15");
        customer1.scriptComponent.Initialize(
        nameOfTheCustomer: "Vicar",
        customerTypeData: "Vital",
        infoBubble: "<b>Vicar</b> \n\n<align=left>•<indent=3.5em>He's wearing full clerical clothing</indent>\n•<indent=3.5em>Carrying a basket of  biscuits, tea bags, milk and coffee</indent>\n•<indent=3.5em>Looks relaxed and isn't in a rush</indent><line-height=0>",
        speeD: 5.5f,
        customerNo: 001,
        questions: new string[]
        {
               "Position yourself in the service area and let him choose which till he wants to use/N",
               "Invite him to use the ASTs for his goods and offer to help him bag his items up/C",
               "The ASTs are all free. Leave him to it whilst you finish tidying up/R"
        },
      result: new string[]
         {
                "He made his way to the kiosk - he looks confused and starts unpacking his products onto the kiosk before you made your way over to him. Not the best experience he's ever had in a Co-op",
                "He's really pleased with the effort you've gone to and talks about the ASTs. You're able to help him use the ASTs and make him feel more confident about using him when he's next in store",
                "You completely missed his approach to the kiosk and he has been stood there for a while. Whilst he is patient and relaxed, he's very unimpressed and tells you how disappointed he is"
         },
       where: "AST",
       SecondC: false,
       MassiveI: false,
       ARS: false,
       chSprite: Resources.Load<Sprite>("Vicar-01"));
        #endregion

        #region Vital 19:30
        customer1 = new CustomerType<Customer1>("Middle Aged Couple", "19:30");
        customer1.scriptComponent.Initialize(
        nameOfTheCustomer: "Middle Aged Couple",
        customerTypeData: "Vital",
        infoBubble: "<b>Middle aged couple</b> \n\n<align=left>•<indent=3.5em>They're dressed casually</indent>\n•<indent=3.5em>Carrying 2 baskets full of shopping</indent>\n•<indent=3.5em>They're deep in conversation and are not in a rush</indent><line-height=0>",
        speeD: 5.5f,
        customerNo: 001,
        questions: new string[]
        {
               "You choose not to disturb them, they are chatting and aren't in a rush. They also have a lot of shopping so will want to use the kiosk/N",
               "You wait for them to approach in their own time, smile and ask if they'd like to use the ASTs, directing them to the right till depending on how they want to pay/C",
               "You greet them as they enter the service area and take one of the baskets for them to the ASTs and say you'll help them put their shopping through/R"
        },
        result: new string[]
         {
                "The customers end up getting confused about the new layout and go through a card only AST, they are paying with cash so the transaction has to be voided and put through a different till. They are a bit annoyed but aren't in a rush so don't mind too much",
                "The customers are grateful for the information on the different till types and appreciate you not interupting their conversation. It turns out that they use the ASTs all the time",
                "The customers didn't want to be disturbed while they did their shop - they tell you that they were having a conversation and you made them feel uncomfortable by assuming they wanted their basket taking"
         },
        where: "AST",
        SecondC: false,
        MassiveI: false,
        ARS: false,
        chSprite: Resources.Load<Sprite>("Couple-01"));
        #endregion

        #region Vital 21:00
        customer1 = new CustomerType<Customer1>("A group of young people", "21:00");
        customer1.scriptComponent.Initialize(
        nameOfTheCustomer: "A group of young people*",
        customerTypeData: "Vital",
        infoBubble: "<b>A group of young people</b> \n\n<align=left>•<indent=3.5em>They're dressed for a night out</indent>\n•<indent=3.5em>Carrying large soft drinks and snacks</indent>\n•<indent=3.5em>They are chatting and laughing loudly</indent><line-height=0>",
        speeD: 5.5f,
        customerNo: 999,
        questions: new string[]
        {
               "You aren't serving any other customers and they all have their own baskets, you let them decide where to queue and serve as required/N",
               "You ask if any of them would like to use the ASTs and offer to serve any others through the kiosk in case they want to purchase alcohol or cigarettes./C",
               "Leave them to it -you have facing up to do, they can go through the ASTs at the same time. They're young so they'll know how to use them/R"
        },
        result: new string[]
         {
                "They come to the ASTs where you're stood and ask for different things at the same time, this means you have to go back and forth to the kiosk and it takes a while to serve them, interventions flash up on the screens and you'll have to call for support",
                "The group splits off depending on whether they want cigarettes or alcohol, two go over to the kiosk and chat whilst you serve them. You validate the age related sale on the attendant screen for the customer using ASTs whilst you are serving",
                "It's chaos! Some customers try to put cash in the card only tills, some double scan items and one forgets to get cigarettes so need to be served again. You end up running over, getting stressed and other customers are now in the area waiting"
         },
        where: "Kiosk", 
        SecondC: false,
        MassiveI: true,
        ARS: true,
        chSprite: Resources.Load<Sprite>("YoungGroup-01"));
        #endregion

        #region Vital 21:00 2
        customer1 = new CustomerType<Customer1>("Middle Aged Man", "21:00");
        customer1.scriptComponent.Initialize(
        nameOfTheCustomer: "Middle Aged Man*",
        customerTypeData: "Vital",
        infoBubble: "<b>Middle aged man</b> \n\n<align=left>•<indent=3.5em>He's dressed in dirty, baggy clothing</indent>\n•<indent=3.5em>Carrying some flowers</indent>\n•<indent=3.5em>He looks relaxed and calm</indent><line-height=0>",
        speeD: 5.5f,
        customerNo: 001,
        questions: new string[]
        {
               "He looks a little shifty so you leave the service area to keep on eye on him/R",
               "It's getting close to closing time and you don't want to be here late so you begin facing up the store/N",
               "Replenish the cigarettes and tobacco and continue to clean the ASTs and make sure that you're keeping your eye out for the customer/C"
        },
        result: new string[]
         {
                "He notices you following them and staring at them. He looks over his shoulder at you. When he gets to the service area you tell him to use the ASTs and he loses his temper. He tells you that technology is stealing jobs and he wishes things could go back to a more simple time",
                "You can't see the service area and  left him stood at the kiosk for around 30 seconds before you noticed he was there. He's frustrated and doesn't look at you or chat to you",
                "Cleaning the ASTs means that you're positioned to help him and your customers notice that the store is clean and tidy. He says, \"There's no chance in hell I'm using those.\" He wants to use the kiosk and it's quiet so you serve him there whilst explaining why the ASTs have been installed"
         },
       where: "Kiosk",
       SecondC: false,
       MassiveI: true,
       ARS: false,
       chSprite: Resources.Load<Sprite>("Hippy_01"));
        #endregion
        
        CreateNewCustomer();
    }

    public void CreateNewCustomer()
    {
        CustomerType<Customer1> customer1;
        ////
        //CustomerNo is used to check who is neutral who is negative in nonvital customers (for giving them the points or taking)
        //111 negative impact
        //112 positive impact 
        ////

        #region NonVital 1 - Image Added done
        customer1 = new CustomerType<Customer1>("1", "NonVitalCustomers");
        customer1.scriptComponent.Initialize(
           nameOfTheCustomer: "1",
           customerTypeData: "NonVitalCustomers", //Kid,Awkward
           infoBubble: "<b>A young man</b> \n\n<align=left>He's buying a meal deal<line-height=0>",
           speeD: 5.5f,
           customerNo: 001,
           questions: new string[] { "-/C", "-/R", "-/N" },
           result: new string[] { "-", "-", "-" },
           where: "AST", 
           SecondC: false,
           MassiveI: false,
           ARS: false,
           chSprite: Resources.Load<Sprite>("GuyOnPhone-01"));
        #endregion

        #region NonVital 2 - Image Added done
        customer1 = new CustomerType<Customer1>("2", "NonVitalCustomers");
        customer1.scriptComponent.Initialize(
           nameOfTheCustomer: "2",
           customerTypeData: "NonVitalCustomers", //Kid,Awkward
           infoBubble: "<b>A young kid</b> \n\n<align=left>He's buying a packet of crisps and a fizzy drink<line-height=0>",
           speeD: 5.5f,
           customerNo: 777,//number for kids
           questions: new string[] { "-/C", "-/R", "-/N" },
           result: new string[] { "-", "-", "-" },
           where: "AST",
           SecondC: false,
           MassiveI: false,
           ARS: false,
           chSprite: Resources.Load<Sprite>("Kid-01"));
        #endregion

        #region NonVital 3 - Image Added done
        customer1 = new CustomerType<Customer1>("3", "NonVitalCustomers");
        customer1.scriptComponent.Initialize(
           nameOfTheCustomer: "3",
           customerTypeData: "NonVitalCustomers", //Kid,Awkward
           infoBubble: "<b>A mother with her child</b> \n\n<align=left>They're buying sweets<line-height=0>",
           speeD: 5.5f,
           customerNo: 001,
           questions: new string[] { "-/C", "-/R", "-/N" },
           result: new string[] { "-", "-", "-" },
           where: "AST",
           SecondC: false,
           MassiveI: false,
           ARS: false,
           chSprite: Resources.Load<Sprite>("MotherAndChild_02"));
        #endregion

        #region NonVital 4 - Image Added
        customer1 = new CustomerType<Customer1>("4", "NonVitalCustomers");
        customer1.scriptComponent.Initialize(
           nameOfTheCustomer: "4",
           customerTypeData: "NonVitalCustomers", //Kid,Awkward
           infoBubble: "<b>An older man</b> \n\n<align=left>He's buying milk and coffee<line-height=0>",
           speeD: 5.5f,
           customerNo: 001,
           questions: new string[] { "-/C", "-/R", "-/N" },
           result: new string[] { "-", "-", "-" },
           where: "AST",
           SecondC: false,
           MassiveI: false,
           ARS: false,
           chSprite: Resources.Load<Sprite>("OldGuy-03"));
        #endregion  

        #region NonVital 5 - Image Added
        customer1 = new CustomerType<Customer1>("5", "NonVitalCustomers");
        customer1.scriptComponent.Initialize(
           nameOfTheCustomer: "5++",
           customerTypeData: "NonVitalCustomers", //Kid,Awkward
           infoBubble: "<b>An older woman</b> \n\n<align=left>She's buying bread, bacon and a Health Lotto ticket<line-height=0>",
           speeD: 5.5f,
           customerNo: 111,//negative customer impact
           questions: new string[] { "-/C", "-/R", "-/N" },
           result: new string[] { "-", "-", "-" },
           where: "Kiosk",
           SecondC: false,
           MassiveI: false,
           ARS: true,
           chSprite: Resources.Load<Sprite>("OldGirl-03"));
        #endregion

        #region NonVital 6 - Image Added
        customer1 = new CustomerType<Customer1>("6", "NonVitalCustomers");
        customer1.scriptComponent.Initialize(
           nameOfTheCustomer: "6++",
           customerTypeData: "NonVitalCustomers", //Kid,Awkward
           infoBubble: "<b>A middle aged man</b> \n\n<align=left>He's buying a bottle of wine and a pizza<line-height=0>",
           speeD: 5.5f,
           customerNo: 001,
           questions: new string[] { "-/C", "-/R", "-/N" },
           result: new string[] { "-", "-", "-" },
           where: "AST",
           SecondC: false,
           MassiveI: false,
           ARS: true,
           chSprite: Resources.Load<Sprite>("BusinessGuy-01"));
        #endregion

        #region NonVital 7 - Image Added
        customer1 = new CustomerType<Customer1>("7", "NonVitalCustomers");
        customer1.scriptComponent.Initialize(
           nameOfTheCustomer: "7",
           customerTypeData: "NonVitalCustomers", //Kid,Awkward
           infoBubble: "<b>A middle aged lady</b> \n\n<align=left>She has a basket of shopping<line-height=0>",
           speeD: 5.5f,
           customerNo: 112,
           questions: new string[] { "-/C", "-/R", "-/N" },
           result: new string[] { "-", "-", "-" },
           where: "AST",
           SecondC: false,
           MassiveI: false,
           ARS: false,
           chSprite: Resources.Load<Sprite>("Oldgirl-01"));
        #endregion

        #region NonVital 8 - Image Added
        customer1 = new CustomerType<Customer1>("8", "NonVitalCustomers");
        customer1.scriptComponent.Initialize(
           nameOfTheCustomer: "8++",
           customerTypeData: "NonVitalCustomers", //Kid,Awkward
           infoBubble: "<b>A young couple</b> \n\n<align=left>They're wanting to buy spirits, cigarettes and multiple Lotto tickets<line-height=0>",
           speeD: 5.5f,
           customerNo: 111,
           questions: new string[] { "-/C", "-/R", "-/N" },
           result: new string[] { "-", "-", "-" },
           where: "Kiosk",
           SecondC: false,
           MassiveI: false,
           ARS: true,
           chSprite: Resources.Load<Sprite>("Couple-02"));
        #endregion

        #region NonVital 9 - Image Added
        customer1 = new CustomerType<Customer1>("9", "NonVitalCustomers");
        customer1.scriptComponent.Initialize(
           nameOfTheCustomer: "9",
           customerTypeData: "NonVitalCustomers", //Kid,Awkward
           infoBubble: "<b>An older man</b> \n\n<align=left>He's buying a newspaper<line-height=0>",
           speeD: 5.5f,
           customerNo: 001,
           questions: new string[] { "-/C", "-/R", "-/N" },
           result: new string[] { "-", "-", "-" },
           where: "AST", 
           SecondC: false,
           MassiveI: false,
           ARS: false,
           chSprite: Resources.Load<Sprite>("OldGuy-04"));
        #endregion

        #region NonVital 10 - Image Added
        customer1 = new CustomerType<Customer1>("10", "NonVitalCustomers");
        customer1.scriptComponent.Initialize(
           nameOfTheCustomer: "10",
           customerTypeData: "NonVitalCustomers", //Kid,Awkward
           infoBubble: "<b>A regular who usually buys a few essentials</b> \n\n<align=left>He normally uses the kiosk out of habit. Today he have milk, eggs and bread<line-height=0>",
           speeD: 5.5f,
           customerNo: 112,
           questions: new string[] { "-/C", "-/R", "-/N" },
           result: new string[] { "-", "-", "-" },
           where: "AST", 
           SecondC: false,
           MassiveI: false,
           ARS: false,
           chSprite: Resources.Load<Sprite>("GuyInSuit-01"));
        #endregion

        #region NonVital 11 - Image Added
        customer1 = new CustomerType<Customer1>("11", "NonVitalCustomers");
        customer1.scriptComponent.Initialize(
           nameOfTheCustomer: "11",
           customerTypeData: "NonVitalCustomers", //Kid,Awkward
           infoBubble: "<b>A young father with a couple of kids</b> \n\n<align=left>He has a basket full of items<line-height=0>",
           speeD: 5.5f,
           customerNo: 112,
           questions: new string[] { "-/C", "-/R", "-/N" },
           result: new string[] { "-", "-", "-" },
           where: "AST",
           SecondC: false,
           MassiveI: false,
           ARS: false,
           chSprite: Resources.Load<Sprite>("ManWithKids-02"));
        #endregion

        #region NonVital 12 - Image Added
        customer1 = new CustomerType<Customer1>("12", "NonVitalCustomers");
        customer1.scriptComponent.Initialize(
           nameOfTheCustomer: "12",
           customerTypeData: "NonVitalCustomers", //Kid,Awkward
           infoBubble: "<b>A Police Officer</b> \n\n<align=left>He's buying some chewing gum and a bottle of water<line-height=0>",
           speeD: 5.5f,
           customerNo: 001,
           questions: new string[] { "-/C", "-/R", "-/N" },
           result: new string[] { "-", "-", "-" },
           where: "AST", 
           SecondC: false,
           MassiveI: false,
           ARS: false,
           chSprite: Resources.Load<Sprite>("Police-03"));
        #endregion

        #region NonVital 13 - Image Added
        customer1 = new CustomerType<Customer1>("13", "NonVitalCustomers");
        customer1.scriptComponent.Initialize(
           nameOfTheCustomer: "13",
           customerTypeData: "NonVitalCustomers", //Kid,Awkward
           infoBubble: "<b>A nurse on her lunch break</b> \n\n<align=left>She's buying a sandwich, some fruit and bus pass<line-height=0>",
           speeD: 5.5f,
           customerNo: 111,
           questions: new string[] { "-/C", "-/R", "-/N" },
           result: new string[] { "-", "-", "-" },
           where: "Kiosk",
           SecondC: false,
           MassiveI: false,
           ARS: false,
           chSprite: Resources.Load<Sprite>("Nurse-02"));
        #endregion

        #region NonVital 14 - Image Added
        customer1 = new CustomerType<Customer1>("14", "NonVitalCustomers");
        customer1.scriptComponent.Initialize(
           nameOfTheCustomer: "14",
           customerTypeData: "NonVitalCustomers", //Kid,Awkward
           infoBubble: "<b>A group of school kids</b> \n\n<align=left>They're buying chocolate and fizzy drinks<line-height=0>",
           speeD: 5.5f,
           customerNo: 777,//number for kids
           questions: new string[] { "-/C", "-/R", "-/N" },
           result: new string[] { "-", "-", "-" },
           where: "AST",
           SecondC: false,
           MassiveI: false,
           ARS: false,
           chSprite: Resources.Load<Sprite>("YoungGroup-02"));
        #endregion

        #region NonVital 15 - Image Added
        customer1 = new CustomerType<Customer1>("15", "NonVitalCustomers");
        customer1.scriptComponent.Initialize(
           nameOfTheCustomer: "15",
           customerTypeData: "NonVitalCustomers", //Kid,Awkward
           infoBubble: "<b>A store colleague</b> \n\n<align=left>She's buying her lunch<line-height=0>",
           speeD: 5.5f,
           customerNo: 001,
           questions: new string[] { "-/C", "-/R", "-/N" },
           result: new string[] { "-", "-", "-" },
           where: "AST",
           SecondC: false,
           MassiveI: false,
           ARS: false,
           chSprite: Resources.Load<Sprite>("Older female colleague"));
        #endregion

        #region NonVital 16 - Image Added
        customer1 = new CustomerType<Customer1>("16", "NonVitalCustomers");
        customer1.scriptComponent.Initialize(
           nameOfTheCustomer: "16",
           //: "Male",
           // age: 20,
           //  difficultyLevel: "easy",
           customerTypeData: "NonVitalCustomers", //Kid,Awkward
           infoBubble: "<b>A young man</b> \n\n<align=left>He's buying some flowers<line-height=0>",
        //   extraSupportInfo: "Andd support button pressed",
           speeD: 5.5f,
           customerNo: 001,
           questions: new string[] { "-/C", "-/R", "-/N" },
           result: new string[] { "-", "-", "-" },
           where: "AST",
           SecondC: false,
           MassiveI: false,
           ARS: false,
           chSprite: Resources.Load<Sprite>("YoungGuy-01"));
        #endregion

        #region NonVital 17 - Image Added
        customer1 = new CustomerType<Customer1>("17", "NonVitalCustomers");
        customer1.scriptComponent.Initialize(
           nameOfTheCustomer: "17",
           customerTypeData: "NonVitalCustomers", //Kid,Awkward
           infoBubble: "<b>A middle aged man</b> \n\n<align=left>He has a basket full of items and he wants to claim his Lotto winnings<line-height=0>",
           speeD: 5.5f,
           customerNo: 111,
           questions: new string[] { "-/C", "-/R", "-/N" },
           result: new string[] { "-", "-", "-" },
           where: "Kiosk",
           SecondC: false,
           MassiveI: false,
           ARS: true,
           chSprite: Resources.Load<Sprite>("Guy-02"));
        #endregion

        #region NonVital 18 - Image Added
        customer1 = new CustomerType<Customer1>("18", "NonVitalCustomers");
        customer1.scriptComponent.Initialize(
           nameOfTheCustomer: "18",
           customerTypeData: "NonVitalCustomers", //Kid,Awkward
           infoBubble: "<b>A young man</b> \n\n<align=left>He's holding a bus pass and an electricity top up key in his hand<line-height=0>",
           speeD: 5.5f,
           customerNo: 111,
           questions: new string[] { "-/C", "-/R", "-/N" },
           result: new string[] { "-", "-", "-" },
           where: "Kiosk", 
           SecondC: false,
           MassiveI: false,
           ARS: false,
           chSprite: Resources.Load<Sprite>("Guy-02"));
        #endregion

        #region NonVital 19 - Image Added
        customer1 = new CustomerType<Customer1>("19", "NonVitalCustomers");
        customer1.scriptComponent.Initialize(
           nameOfTheCustomer: "&19",//& used for detecting the wheelchair users
           customerTypeData: "NonVitalCustomers", //Kid,Awkward
           infoBubble: "<b>A wheelchair user</b> \n\n<align=left>He has a basket full of items<line-height=0>",
           speeD: 5.5f,
           customerNo: 001,
           questions: new string[] { "-/C", "-/R", "-/N" },
           result: new string[] { "-", "-", "-" },
           where: "Kiosk",
           SecondC: false,
           MassiveI: false,
           ARS: false,
           chSprite: Resources.Load<Sprite>("Wheelchair-03"));
        #endregion

        #region NonVital 20 - Image Added
        customer1 = new CustomerType<Customer1>("20", "NonVitalCustomers");
        customer1.scriptComponent.Initialize(
           nameOfTheCustomer: "20++",
           customerTypeData: "NonVitalCustomers", //Kid,Awkward
           infoBubble: "<b>A couple</b> \n\n<align=left>They have a trolley full of items including boxes of beer and produce<line-height=0>",
           speeD: 5.5f,
           customerNo: 111,
           questions: new string[] { "-/C", "-/R", "-/N" },
           result: new string[] { "-", "-", "-" },
           where: "Kiosk",
           SecondC: false,
           MassiveI: false,
           ARS: true,
           chSprite: Resources.Load<Sprite>("Couple-03"));
        #endregion
    }
}
