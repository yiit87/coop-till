﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class CustomerType<T> where T : CustomerTypes
{
    public GameObject go;
    public T scriptComponent;

    public CustomerType(string name,string dayTime)
    {
        GameObject spawn1 = GameObject.Find("VitalCustomerTimes");
        GameObject spawn2 = GameObject.Find("NonVitalCustomers");
       // GameObject spawn3 = GameObject.FindGameObjectWithTag("SpawnPoolNight");
        GameObject spawn4 = GameObject.FindGameObjectWithTag("SupportSpawn");

        go = new GameObject(name);
        //go.transform.forward = Vector3.up;
        Debug.DrawRay(go.transform.localPosition, go.transform.forward,Color.red);
        go.transform.localScale = Vector3.one * 3;

        #region Check where the customer is belong to as list. 
        if (dayTime == "7:00")
        {
            go.transform.position = spawn1.transform.GetChild(0).position;
            go.transform.SetParent(spawn1.transform.GetChild(0).transform);

        }
        else if (dayTime == "7:30")
        {
            go.transform.position = spawn1.transform.GetChild(1).position;
            go.transform.SetParent(spawn1.transform.GetChild(1).transform);

        }
        else if (dayTime == "8:30")
        {
            go.transform.position = spawn1.transform.GetChild(2).position;
            go.transform.SetParent(spawn1.transform.GetChild(2).transform);

        }
        else if (dayTime == "9:00")
        {
            go.transform.position = spawn1.transform.GetChild(3).position;
            go.transform.SetParent(spawn1.transform.GetChild(3).transform);

        }
        else if (dayTime == "10:00")
        {
            go.transform.position = spawn1.transform.GetChild(4).position;
            go.transform.SetParent(spawn1.transform.GetChild(4).transform);
        }
        else if (dayTime == "11:30")
        {
            go.transform.position = spawn1.transform.GetChild(5).position;
            go.transform.SetParent(spawn1.transform.GetChild(5).transform);

        }
        else if (dayTime == "12:00")
        {
            go.transform.position = spawn1.transform.GetChild(6).position;
            go.transform.SetParent(spawn1.transform.GetChild(6).transform);

        }
        else if (dayTime == "12:30")
        {
            go.transform.position = spawn1.transform.GetChild(7).position;
            go.transform.SetParent(spawn1.transform.GetChild(7).transform);

        }
        else if (dayTime == "12:45")
        {
            go.transform.position = spawn1.transform.GetChild(8).position;
            go.transform.SetParent(spawn1.transform.GetChild(8).transform);

        }
        else if (dayTime == "13:00")
        {
            go.transform.position = spawn1.transform.GetChild(9).position;
            go.transform.SetParent(spawn1.transform.GetChild(9).transform);

        }
        else if (dayTime == "15:00")
        {
            go.transform.position = spawn1.transform.GetChild(10).position;
            go.transform.SetParent(spawn1.transform.GetChild(10).transform);

        }
        else if (dayTime == "16:00")
        {
            go.transform.position = spawn1.transform.GetChild(11).position;
            go.transform.SetParent(spawn1.transform.GetChild(11).transform);

        }
        else if (dayTime == "17:00")
        {
            go.transform.position = spawn1.transform.GetChild(12).position;
            go.transform.SetParent(spawn1.transform.GetChild(12).transform);

         }
        else if (dayTime == "17:30")
        {
            go.transform.position = spawn1.transform.GetChild(13).position;
            go.transform.SetParent(spawn1.transform.GetChild(13).transform);

        }
        else if (dayTime == "18:00")
        {
            go.transform.position = spawn1.transform.GetChild(14).position;
            go.transform.SetParent(spawn1.transform.GetChild(14).transform);

        }
        else if (dayTime == "19:15")
        {
            go.transform.position = spawn1.transform.GetChild(15).position;
            go.transform.SetParent(spawn1.transform.GetChild(15).transform);

        }
        else if (dayTime == "19:30")
        {
            go.transform.position = spawn1.transform.GetChild(16).position;
            go.transform.SetParent(spawn1.transform.GetChild(16).transform);

        }
        else if (dayTime == "21:00")
        {
            go.transform.position = spawn1.transform.GetChild(17).position;
            go.transform.SetParent(spawn1.transform.GetChild(17).transform);

        }
        else if (dayTime == "NonVitalCustomers")
        {
            go.transform.position = spawn2.transform.position;
            go.transform.SetParent(spawn2.transform);
 //           type = Type.NonVital;
        }
        else if(dayTime == "Support")
        {
            go.transform.position = spawn4.transform.position;
            go.transform.SetParent(spawn4.transform);
//            type = Type.Support;
        }
        #endregion  

        scriptComponent = go.AddComponent<T>();
    }
}

public enum State
{
    Pool,
    Spawned,
    InFirstQueue,
    GoKioskQueue,
    InKioskQueue,
    AtKiosk,
    GoAstQueue,
    InAstQueue,
    AtAst,
    Leaving
};

public abstract class CustomerTypes : MonoBehaviour
{
    public string customerName;
    public string customerType;
    public string informationBuble;
    public int customerNo;
    public string[] customerVitalQuestions;
    public string[] customerVitalResults;
    public string WhereToGo;
    public bool SecondColleauge;
    public bool MassiveImpact;
    public bool ARSButton;
    public Animator anim;

    public float speed;
    public SpriteRenderer characterTexture;
    public SpriteRenderer red;

    public NavMeshAgent agent;
    public BoxCollider colliderB;
    public Rigidbody rigid;
    public CustomerAI AI;

    public State state;
    
    void Awake()
    {
        state = State.Pool;
   
        GameObject firstChild = new GameObject();
        firstChild.transform.localScale = Vector3.one * 3;
        firstChild.transform.localPosition = new Vector3(0, 0f, 0);
        firstChild.transform.SetParent(gameObject.transform);
        characterTexture = firstChild.AddComponent<SpriteRenderer>();

        anim = firstChild.AddComponent<Animator>();
        anim.runtimeAnimatorController = Resources.Load<RuntimeAnimatorController>("WalkAnimationController");
        
        GameObject go = new GameObject();
        
        go.transform.SetParent(firstChild.transform);
        go.transform.localPosition = new Vector3(0, 5f, 0);
        go.transform.localScale = Vector3.one/2.5f;
       
        red = go.AddComponent<SpriteRenderer>();

        if (firstChild.transform.parent.name == "Support")
        {
            firstChild.transform.localScale = Vector3.one*1.2f;
            Color tmp = characterTexture.color;
            tmp.a = 0f;
            characterTexture.color = tmp;
        }

        red.sortingLayerName = "11";

        colliderB = gameObject.AddComponent<BoxCollider>();
        // creating navMeshAgent here generates a warning (not close enough to the Navmesh)
        //agent = gameObject.AddComponent<NavMeshAgent>();
        rigid = gameObject.AddComponent<Rigidbody>();
        AI = gameObject.AddComponent<CustomerAI>();
        
        rigid.useGravity = false;
        rigid.isKinematic = true;

        if (gameObject.name == "Support")
        {
            gameObject.tag = "Support";            
        }
        else
        {
            gameObject.tag = "Customer";
        }

    }

    public void AddAgent()
    {
        agent = gameObject.AddComponent<NavMeshAgent>();
        agent.baseOffset = 1;
        agent.angularSpeed = 50;
        agent.acceleration = 50;
        agent.stoppingDistance = 2;
        agent.autoBraking = true;
        agent.radius = 0.39f;
        agent.height = 0.45f;
        agent.obstacleAvoidanceType = ObstacleAvoidanceType.NoObstacleAvoidance;

        // Extra initialisation from CustomerAI.cs
        agent.updateRotation = false;
        agent.speed = CM_Handler.Instance.speed;
        //agent.angularSpeed = CM_Handler.Instance.turnSpeed;
       // agent.acceleration = CM_Handler.Instance.acceleration;

    }

    //Insert all unique values to be determined at instantiateion here
    public virtual void Initialize(string nameOfTheCustomer, string customerTypeData, string infoBubble, float speeD, int customerNo, string[] questions, string[] result, string where,bool SecondC, bool MassiveI,bool ARS, Sprite chSprite)
    {
        customerName = nameOfTheCustomer;
        customerType = customerTypeData;
        informationBuble = infoBubble;
        speed = speeD;
        this.customerNo = customerNo;
        WhereToGo = where;
        customerVitalQuestions = questions;
        customerVitalResults = result;
        characterTexture.sprite = chSprite;
        SecondColleauge = SecondC;
        MassiveImpact = MassiveI;
        ARSButton = ARS;
    }
}
public class Customer1 : CustomerTypes
{
    void Start()
    {
        //https://www.youtube.com/watch?v=jHNVrkSyf78
        //Check this link for collider stuff later on
        //it isnt important at the moment
        transform.localScale = new Vector3(1, 1, 1);
       // gameObject.transform.position = new Vector3(gameObject.transform.localPosition.x, 0f, gameObject.transform.localPosition.z);
    }
}

