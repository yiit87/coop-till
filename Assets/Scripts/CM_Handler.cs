﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum DayState
{
    Busy,
    Quiet,
}

[System.Serializable]
public class TimePoints
{
    public float Between1;
    public float Between2;
}

[RequireComponent(typeof(AddCustomer))]
[RequireComponent(typeof(ResultScreen))]
[RequireComponent(typeof(QuestionManager))]
[RequireComponent(typeof(CustomerInformation))]
[RequireComponent(typeof(BoxCollider))]
[RequireComponent(typeof(PointSystem))]
public class CM_Handler : MonoBehaviour
{
    public static CM_Handler Instance;

    [Header("Customer Enters The Shop Values")]
    public float speed;
    private float turnSpeed = 100f; //these 2 values (turn speed and acceleration) is changing automatically in the code CustomerAI.cs
    private float acceleration = 100f;
    public float customerMoveToFrontOfTheQueueAfterThisMuchSeconds;

    [Header("Customer Waits On The Till This Much")]
    public float CorrectTillWaitTime;
    public float WrongTillWaitTime;

    [Header("Calculate The Game time")]
    public float TotalGameTime;

    [HideInInspector]
    public float ShopOpeningTime = 7f;
    public float TotalHoursTheShopOpen;
    public float ClosingTime;

    [Header("Customer Spawning Times")]
    public List<float> customerSpawningTimes = new List<float>();

    [Header("Vital customer after questions are answered how long they should wait")]
    public float QuestionScreenDissapearTime;

    [Header("Customer Speeds After The Till Choice Made")]
    public float CorrectTillCustomerSpeed;
    public float FalseTillCustomerSpeed;

    [Header("+2 customers in the queue SO remove points every this second")]
    public float PunishmentTimerCountDown = 1f;

    /// <summary>
    /// Adjustable Time and speed Values ABOVE
    /// </summary>
    ///



    [Header("Second Colleague Message Box")]
    public GameObject SecondColleagueInfoBox;

    [Header("Queue Targets")]
    public List<GameObject> targetLocations = new List<GameObject>();//for customer
    public List<GameObject> supportTargetLocation = new List<GameObject>();//for support mate

    /*[HideInInspector]
    [Header("Total Customers In The Queue")]*/
    public List<GameObject> waitingQueuePositionList = new List<GameObject>();

    [Header("Left and Right buttons")]
    public GameObject LeftTarget;
    public GameObject RightTarget;

    /*[HideInInspector]*/
    [Header("Boleans For Tills")]
    public GameObject left = null;
    /*[HideInInspector]*/
    public GameObject right = null;
    [HideInInspector]
    public bool support = false;
    [HideInInspector]
    public bool fillSystemWork = false;
    //[HideInInspector]
    //float fillAmountForsupport = 0.0f;

    [Header("Support Button Image - CHECK 'SupportSystemCode' GAMEOBJECT FOR MORE DATA")]
    public GameObject SupportButton;
    public GameObject SupGOParent;

    [Header("Second Colleague")]
    public GameObject SecondColleague;
    public bool SecondColleagueInUse;
    public float HowLongItShouldStay;

    [Header("UI Elements")]
    public GameObject HappynessMeterSlider;
    public GameObject HappynessMeterChangeFaces;

    public List<Vector2> ChangeFaces;

    public GameObject PlusHappyness;
    public GameObject MinusHappyness;
    public GameObject LeftButton;//Send the customer to kiosk
    public GameObject RightButton;//send the customer to AST
    public Text GameTimeUI; //The counting down time
    public GameObject ARS;
    public GameObject PauseButton;
    public GameObject PauseScreenPanel;
    public GameObject OneHourRemaining;
    public float OneHourRemainingVisibilityTime;
    public List<GameObject> WelcomeScreenImages = new List<GameObject>();

    [Header("DO NOT CHANGE THIS - ITS HERE FOR TESTING PURPOSES!")]
    public float GameTimeInSeconds = 120f;

    [Header("Day Time")]
    public GameObject Clock;
    public GameObject Hour;
    public GameObject Min;

    public float customerSpawnTimer;//A timer to determine customer spawning times

    GameObject spawnPoint; //The first point where the customer start walking towards shop
    DayState day;//current day state

    //List pools for customers that is exist
    public List<GameObject> NonVitalCustomers = new List<GameObject>();

    [Header("Parent Of All Vital Customers")]
    public GameObject Vitals;

    List<GameObject> SupportMateList = new List<GameObject>();//for support agent


    [HideInInspector]
    public List<GameObject> AvailableTills_AST_1 = new List<GameObject>();//Lists for Available and unavailable ast tills
    [HideInInspector]
    public List<GameObject> UnavailableTill_AST_1 = new List<GameObject>();
    [HideInInspector]
    public List<GameObject> AvailableTills_AST_2 = new List<GameObject>();//Lists for Available and unavailable ast tills
    [HideInInspector]
    public List<GameObject> UnavailableTill_AST_2 = new List<GameObject>();
    [HideInInspector]
    public List<GameObject> AvailableTills_Kiosk = new List<GameObject>(); //Lists for Available and unavailable kiosk tills
    [HideInInspector]
    public List<GameObject> UnavailableTill_Kiosk = new List<GameObject>();

    [HideInInspector]
    [Header("Game Pause System")]
    public bool GamePause = false;

    [Header("Game Information GO")]
    public GameObject welcomeUI2;
    public GameObject welcomeText;


    //  [Header("Game Session Statistics")]
    [HideInInspector]
    public int TotalCustomerServed;
    [HideInInspector]
    public float TotalHappyness;
    [HideInInspector]
    public int TotalNumberOfSupoortButtonPresses;
    [HideInInspector]
    public int TotalCorrectAnswers;
    [HideInInspector]
    public int TotalWrongAnswers;
    [HideInInspector]
    public int TotalNaturalAnswers;
    [HideInInspector]
    public int TotalCorrectTill;
    [HideInInspector]
    public int TotalWrongTill;

    [Header("To delete the customer or support")]
    public GameObject Exit;

    [HideInInspector]
    public bool customerGoingHome = false;


    [Header("Total Number Of Customer Spawned")]
    public int TotalCustomerSpawned;
    int NonVitalCustomerCounter;
    bool firstTime = false;

    [Header("New QueueSystem for Kiosk and AST")]
    public GameObject KioskQueue;
    public GameObject ASTQueue;

    int VitalOrNonVital;
    bool VitalIsSpawned;

    [HideInInspector]
    public bool ARSButtonPressed;
    public bool ARSReadyToCheck;

    bool pointTaken; //ARS points


    [HideInInspector]
    public bool NautralAnswer = false;
    int countTouches = 0;

    [Header("Coop second Colleugue")]
    public GameObject CoopFirstEmployee;
    Animator FirstEmployeeAnim;

    [HideInInspector]
    public float SafetyTillSystemResetValueSec1 = 15f;
    [HideInInspector]
    public float SafetyTillSystemResetValueSec2 = 15f;

    //string nameOftheCustomer;
    float timerSave = 0;
    //GameObject dontDestroy;
    bool TutorialButtonClicked;
    GameObject nonvitalGo;
    GameObject supportMateCustomerGO;
    float saveHowLong;
    public List<GameObject> HowManyARSCustomer = new List<GameObject>();
    private Vector3 SupportPersonMainLocation = new Vector3(7.899994f, 7.001202f, 23.29999f);

    public GameObject TapToDismiss;
    public GameObject CheckTheParentOfDismissMessage;
    float countDownForTapToDismiss;
    public GameObject Hidethesup;

    [Header("Detect the non vital customers")]
    public GameObject NonVCustomerNumber;


    public bool RecycleSpawning=false;
    //NavMeshAgent agentGeneric;      // JY NOTE - agentGeneric was never assigned to

    void Awake()
    {
        TotalCustomerServed =
        TotalNumberOfSupoortButtonPresses =
        TotalCorrectAnswers =
        TotalWrongAnswers =
        TotalNaturalAnswers =
        TotalCorrectTill =
        TotalWrongTill =

        NonVitalCustomerCounter = 0;
        TotalHappyness = 0f;

        timerSave = PunishmentTimerCountDown;//for punisment points

        VitalIsSpawned =
        TutorialButtonClicked =
        ARSButtonPressed =
        ARSReadyToCheck =
        customerGoingHome = false;

        PauseScreenPanel.SetActive(false);
        SecondColleagueInfoBox.SetActive(false);

        //Happyness meter
        HappynessMeterSlider.GetComponent<Slider>().maxValue = 100;
        HappynessMeterSlider.GetComponent<Slider>().minValue = 0;
        HappynessMeterSlider.GetComponent<Slider>().value = 50;
        //
        GameTimeInSeconds = TotalGameTime;
        welcomeText.GetComponent<Text>().text = "Welcome to Your Service Your Way! \n\nYou will be presented with an accelerated view of a typical day in store, and must decide where and how to best direct customers in the new store layout.";

        welcomeUI2.SetActive(true);//the first window that is welcoming the user

        Instance = this;

        day = DayState.Busy; //day time system
        GamePause = true; //Game starts as paused

        AvailableTills_AST_1.Add(RightTarget.transform.GetChild(0).gameObject);
        AvailableTills_AST_2.Add(RightTarget.transform.GetChild(1).gameObject);

        for (int i = 0; i < LeftTarget.transform.childCount; i++)
        {
            AvailableTills_Kiosk.Add(LeftTarget.transform.GetChild(i).gameObject);
        }
    }

    void Start()
    {
        Hidethesup.SetActive(true);
        //dontDestroy = GameObject.Find("DontDestroyObject");
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        customerSpawnTimer = 0f;
        spawnPoint = GameObject.FindGameObjectWithTag("SpawnPoint");

        SupportButton.GetComponent<Button>().interactable =
        pointTaken =
        SecondColleagueInUse =
        support = false;

        SecondColleague.GetComponent<Animator>().enabled = true;
        if (SupGOParent.transform.GetChild(0).GetComponent<SpriteRenderer>())
        {
            SupGOParent.transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = false;
        }

        OneHourRemaining.SetActive(false);


        //fillAmountForsupport = 1f;

        //Customer pools
         nonvitalGo = NonVCustomerNumber;// GameObject.Find("NonVitalCustomers");
        // JY NOTE nonvitalGo and NonVCustomerNumber are the same gameObject
 
        for (int i = 0; i < nonvitalGo.transform.childCount; i++)
        {
            NonVitalCustomers.Add(nonvitalGo.transform.GetChild(i).gameObject);
        }
        //Debug.Log(nonvitalGo.transform.childCount + " Total Non vital customers");


        GameObject go = GameObject.FindGameObjectWithTag("SupportSpawn");

        for (int i = 0; i < go.transform.childCount; i++)
        {
            SupportMateList.Add(go.transform.GetChild(i).gameObject);
        }

        float aspectRatio = (float)Screen.width / Screen.height;

        if (aspectRatio > 1.9f)
        {
            GameObject cam = GameObject.Find("Main Camera");
            cam.GetComponent<Transform>().position = new Vector3(29.9f, 40.3f, -43.2f);
        }

        FirstEmployeeAnim = CoopFirstEmployee.GetComponent<Animator>();
        FirstEmployeeAnim.enabled = false;
        countDownForTapToDismiss = QuestionScreenDissapearTime;
    }

    void OnTriggerEnter(Collider col)
    {
        // Entering first queue
        if (col.name != "Support")
        {
            //Debug.Log("Initial add " + col.gameObject.name + " @position " + waitingQueuePositionList.Count);
            Customer1 script = col.gameObject.GetComponent<Customer1>();
            //Debug.Log("Join first queue " + script.customerName + " @ " + script.state);
            Debug.AssertFormat(script.state == State.Spawned, "Enter queue area with state " + script.state + ", name " + script.customerName);
            script.state = State.InFirstQueue;
            if (waitingQueuePositionList.Contains(col.gameObject))
            {
                Debug.Log("ATTEMPTING TO DUPLICATE AN ENTRY IN THE WAITING LIST " + col.gameObject.name + " @position " + waitingQueuePositionList.Count);
            }
            else
            {
                waitingQueuePositionList.Add(col.gameObject);

                if (col.GetComponent<Customer1>().customerName.Contains("&"))
                {
                    col.transform.GetChild(0).GetComponent<Animator>().speed = 0f;
                }
            }
        }
    }

    public void SendBackToFirstQueue(GameObject customer)
    {
        Customer1 customer1 = gameObject.GetComponent<Customer1>();
        for (int i = 0; i < waitingQueuePositionList.Count; ++i)
        {
            if (waitingQueuePositionList[i] == gameObject)
            {
                Debug.Log(customer1.customerName + " is in the queue @ position " + i + " - send them back!");
                gameObject.GetComponent<NavMeshAgent>().SetDestination(KioskQueue.transform.GetChild(i).transform.position);
                return;
            }
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.name != "Support")
        {
            //Debug.Log("Exit trigger " + col.name);
            Customer1 customer1 = col.gameObject.GetComponent<Customer1>();
            if (customer1.state == State.InFirstQueue)
            {
                // Have seen this. It may be the result of a collision?
                Debug.Log(customer1.customerName + " exited queue unexpectedly");
                SendBackToFirstQueue(col.gameObject);
                return;
            }
            Debug.AssertFormat(customer1.state == State.GoKioskQueue || customer1.state == State.GoAstQueue, customer1.customerName + " leaving main queue in state " + customer1.state);

            if (right == col.gameObject)
            {
                col.gameObject.GetComponentInChildren<SpriteRenderer>().sortingLayerName = "6";
            }
            else if (left == col.gameObject)
            {
                col.gameObject.GetComponentInChildren<SpriteRenderer>().sortingLayerName = "5";
            }

            //detect the ars button from customer details
            if (col.GetComponent<Customer1>().ARSButton == true )
            {
                HowManyARSCustomer.Add(col.gameObject);
            }

            if (left == col.gameObject)
            {
                WhichTillToGo("Kiosk");
            }

            else if (right == col.gameObject)
            {
                WhichTillToGo("AST");
            }

            TotalCustomerServed++;
            StartCoroutine(DontDeleteTheQueueListDirectly());
        }
    }

    IEnumerator DontDeleteTheQueueListDirectly()
    {
        yield return new WaitForSeconds(customerMoveToFrontOfTheQueueAfterThisMuchSeconds);
        //Debug.Log("Remove " + waitingQueuePositionList[0].name);
        if (left == waitingQueuePositionList[0])
        {
            Debug.Log("Clear left");
            left = null;
        }
        else if (right == waitingQueuePositionList[0])
        {
            Debug.Log("Clear right");
            right = null;
        }
        waitingQueuePositionList.RemoveAt(0);
    }

    // Assist QuestionManager to not display 'questions' again after an option was chosen
    public bool AlreadyChosenOption()
    {
        return left == waitingQueuePositionList[0] || right == waitingQueuePositionList[0];
    }

    void OnTriggerStay(Collider col)
    {
        //if game is not paused
        if (!GamePause)
        {
            //if time is bigger then 0 (so game havent finished)
            if (GameTimeInSeconds >= 0)
            {
                NavMeshAgent nav;  //nav mesh target system for queue or button presses

                //Sending customers to different locations
                for (int i = 0; i < waitingQueuePositionList.Count; i++)
                {
                    if (i == 0 && left == waitingQueuePositionList[0])
                    {
                        GameObject go = waitingQueuePositionList[0].gameObject;
                        Customer1 script = go.GetComponent<Customer1>();

                        Debug.AssertFormat(script.state == State.InFirstQueue || script.state == State.GoKioskQueue || script.state == State.InKioskQueue, script.customerName + " stay in main queue with state " + script.state);
                       
                        if (script.state == State.GoKioskQueue || script.state == State.InKioskQueue)
                        {
                            // Nothing to do, already leaving
                        }
                        else if (KioskQueueSystem.Instance.KioskWaitingQueueList.Count < 2)
                        {
                            //Debug.Log("Sending " + go.name + " to kiosk @ position " + KioskQueueSystem.Instance.KioskWaitingQueueList.Count);
                            Debug.AssertFormat(script.state == State.InFirstQueue, "'" + script.customerName + "' in wrong state to send - " + script.state);
                            script.state = State.GoKioskQueue;
                            SetTargets(waitingQueuePositionList[0].GetComponent<NavMeshAgent>(),
                                        KioskQueue.transform.GetChild(KioskQueueSystem.Instance.KioskWaitingQueueList.Count).transform.gameObject);
                            go.GetComponentInChildren<SpriteRenderer>().sortingLayerName = "5";
                        }
                        else
                        {
                            // No space in the kiosk queue, wait
                            //Debug.Log("Not sending " + go.name + " to kiosk @ position " + KioskQueueSystem.Instance.KioskWaitingQueueList.Count);
                        }
                    }
                    else if (i == 0 && right == waitingQueuePositionList[0])
                    {
                        GameObject go = waitingQueuePositionList[0].gameObject;
                        Customer1 script = go.GetComponent<Customer1>();

                        if (waitingQueuePositionList[0].GetComponent<NavMeshAgent>().velocity == Vector3.zero)
                        {
                            if (script.state == State.GoAstQueue)
                            {
                                // Nothing to do, already leaving
                            }
                            else if (ASTQueueSystem.Instance.ASTWaitingAreaList.Count < 2)
                            {
                                Debug.AssertFormat(go.GetComponent<Customer1>().state == State.InFirstQueue, "state of " + script.customerName + " is " + script.state);
                                go.GetComponent<Customer1>().state = State.GoAstQueue;
                                SetTargets(waitingQueuePositionList[0].GetComponent<NavMeshAgent>(), ASTQueue.transform.GetChild(ASTQueueSystem.Instance.ASTWaitingAreaList.Count).transform.gameObject);

                                waitingQueuePositionList[0].GetComponent<NavMeshAgent>().stoppingDistance = 10f;
                                ASTQueueSystem.Instance.ASTWaitingAreaList.Add(waitingQueuePositionList[0].gameObject);
                            }
                        }
                    }
                    else if (col.gameObject == waitingQueuePositionList[i].gameObject)
                    {
                        nav = col.GetComponent<NavMeshAgent>();
                        //Debug.Log("Set to " + i + ", " + col.gameObject.name);
                        nav.SetDestination(targetLocations[i].transform.position);
                    }
                }
            }
        }

        //if the queue is full and there are more customers this is what happens..
        //NoTE: Add -10 to the timer (or whatever the customer wants) if they want
        //if we do this we also need to change the clock animation system
        if (waitingQueuePositionList.Count > targetLocations.Count)
        {
            Debug.Log("NEVER SEEN THIS");
            Destroy(waitingQueuePositionList[waitingQueuePositionList.Count - 1].gameObject);
            waitingQueuePositionList.RemoveAt(waitingQueuePositionList.Count - 1);
        }
    }

    public void SetTargets(NavMeshAgent agent, GameObject targetList)
    {
        StartCoroutine(SetLeftRightBoolToFalse());
        // Debug.Log(agent.gameObject.name + " name of the customer /" + targetList.transform.gameObject.name + " taregt location name");
        agent.SetDestination(targetList.transform.position);

    }

    void HappynessMeterAddOrRemove(int value)
    {
        HappynessMeterSlider.GetComponent<Slider>().value += value;
    }

    void Update()
    {
        //Debug.Log(FirstEmployeeAnim.GetCurrentAnimatorStateInfo(0).normalizedTime + " normalized time value");

        //ARS Active or not system depending of the ars customer number in the list
        #region ARS customer list
        if (HowManyARSCustomer.Count > 0)
        {
            if (ARS.GetComponent<Animator>().GetBool("isActivatedNow") == false)
            {
                ARS.GetComponent<Animator>().SetBool("isActivatedNow", true);
            }
        }
        else
        {
            if (ARS.GetComponent<Animator>().GetBool("isActivatedNow") == true)
            {
                ARS.GetComponent<Animator>().SetBool("isActivatedNow", false);
            }
        }
        #endregion

        TotalHappyness = HappynessMeterSlider.GetComponent<Slider>().value;

        if (!GamePause)
        {
            if (FirstEmployeeAnim.GetCurrentAnimatorStateInfo(0).IsName("WaitMovement"))
            {
                FirstEmployeeAnim.SetBool("ReverseNow", false);
            }

            //Hide the sprite of the support
            if (GameTimeInSeconds < TotalGameTime - 2)
            {
                Hidethesup.SetActive(false);
            }
            #region Second/First colleague animations
            if (!SecondColleagueInUse && SecondColleague.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("New State"))
            {
                SecondColleague.transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = false;
                // SupportButton.GetComponent<Button>().interactable = true;
            }
            else if (SecondColleagueInUse && SecondColleague.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("SecondColleagueAnim"))
            {
                SecondColleague.transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = true;
            }
            SecondColleague.GetComponent<Animator>().speed = 1;
            SecondColleague.transform.GetChild(0).GetComponent<Animator>().speed = 1f;

            //Debug.Log("Anim: set speed 1");
            FirstEmployeeAnim.speed = 1;
            CoopFirstEmployee.transform.GetChild(0).GetComponent<Animator>().speed = 1;
            #endregion

            #region Game Time count down
            //GameTime
            GameTimeInSeconds -= 1 * Time.deltaTime;
            #endregion

            #region Second Colleague set area

            if (SecondColleagueInUse)
            {
                if (!support)
                {
                    if (waitingQueuePositionList.Count > 0)
                    {
                        if (waitingQueuePositionList[0].gameObject == supportMateCustomerGO)
                        {
                            if (SecondColleague.GetComponent<Animator>().GetBool("isActive") == false)
                            {
                                SecondColleague.GetComponent<Animator>().SetBool("isActive", true);

                                SecondColleague.transform.GetChild(0).GetComponent<Animator>().SetBool("isWalking", true);
                            }
                        }
                    }
                    if (SecondColleague.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("New State 0"))
                    {
                        SecondColleague.transform.GetChild(0).GetComponent<Animator>().SetBool("isWalking", false);
                    }
                }
            }
            if (customerGoingHome)
            {
                if (SecondColleague.GetComponent<Animator>().GetBool("isActive"))
                {
                    SecondColleague.GetComponent<Animator>().SetBool("isActive", false);
                    SecondColleague.transform.GetChild(0).GetComponent<Animator>().SetBool("isWalking", true);

                    SecondColleagueInUse = false;
                    customerGoingHome = false;
                    supportMateCustomerGO = null;
                }
                if (SecondColleague.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("New State"))
                {
                    SecondColleague.transform.GetChild(0).GetComponent<Animator>().SetBool("isWalking", false);
                }
            }
            #endregion

            #region Clock animation area including 1 h remaining warning

            //Games clock animation part..
            float Difference = TotalGameTime - GameTimeInSeconds;
            float divideValue = TotalGameTime / TotalHoursTheShopOpen;
            float currentTimeOfTheDay = (Difference / divideValue) + ShopOpeningTime;

            float Xvalue = (360 / 12) * (currentTimeOfTheDay - 6);
            Hour.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, -Xvalue);

            float decimalX = currentTimeOfTheDay % 1f;
            float YValue = 360 * (decimalX);
            Min.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, (-YValue - 180f));

            if (GameTimeInSeconds <= 12)
            {

            }

            if (currentTimeOfTheDay >= (ClosingTime - 1))
            {
                OneHourRemainingVisibilityTime -= 1 * Time.deltaTime;

                OneHourRemaining.SetActive(true);

                Clock.GetComponent<Animator>().speed = .75f;
                Clock.GetComponent<Animator>().SetTrigger("isAlmostDone");

                if (OneHourRemainingVisibilityTime <= 0)
                {
                    OneHourRemaining.SetActive(false);
                }
            }
            #endregion

            #region Safety code below
            //Safety code below. Due to an unknown reason sometimes (out of 30 times maybe 2 times) customer goes to a till that is already occopied
            //Because of this problem tills get stuck as unavailable. The code below will wait 20 seconds at least to make that till available again in case
            //that error occurs
            if (UnavailableTill_AST_1.Count > 0)
            {
                SafetyTillSystemResetValueSec1 -= 1 * Time.deltaTime;

                if (SafetyTillSystemResetValueSec1 <= 0)
                {
                    AvailableTills_AST_1.Add(UnavailableTill_AST_1[0].gameObject);
                    UnavailableTill_AST_1.Clear();
                    //20 seconds it takes at the moment for a customer to spend time if the customer is going to the wrong ast
                    SafetyTillSystemResetValueSec1 = 15f;
                }
            }
            else
            {
                SafetyTillSystemResetValueSec1 = 15f;
            }
            if (UnavailableTill_AST_2.Count > 0)
            {
                SafetyTillSystemResetValueSec2 -= 1 * Time.deltaTime;

                if (SafetyTillSystemResetValueSec2 <= 0)
                {
                    AvailableTills_AST_2.Add(UnavailableTill_AST_2[0].gameObject);
                    UnavailableTill_AST_2.Clear();
                    SafetyTillSystemResetValueSec2 = 15f;
                }
            }
            else
            {
                SafetyTillSystemResetValueSec2 = 15f;
            }
            /////////////////////////////////////////////////////////
            #endregion

            #region ARS point take down if it doesnt pressed
            //check if ars button pressed. This check happens after customer leaves the till
            if (!ARS.GetComponent<Animator>().GetBool("isActivatedNow"))
            {
                ARSButtonPressed = false;
            }

            if (!ARSButtonPressed && !pointTaken)
            {
                if (ARSReadyToCheck)
                {
                    HappynessMeterChangeFaces.GetComponent<Animator>().SetTrigger("HappySliderBounce");
                    MinusHappyness.GetComponent<Animator>().SetTrigger("isMinusRed");
                    HappynessMeterAddOrRemove(PointSystem.Instance.RemovePointForAgeButtonNOTPressed);//-5 default
                    pointTaken = true;
                    ARSReadyToCheck = false;
                }
            }
            #endregion

            #region Happyness Faces
            if (TotalHappyness < ChangeFaces[0].x && TotalHappyness > ChangeFaces[0].y)
            {
                HappynessMeterChangeFaces.GetComponent<Image>().sprite = Resources.Load<Sprite>("SadHappyAnim_0001");
            }
            else if (TotalHappyness > ChangeFaces[1].y && TotalHappyness < ChangeFaces[1].x)
            {
                HappynessMeterChangeFaces.GetComponent<Image>().sprite = Resources.Load<Sprite>("SadHappyAnim_0020");
            }
            else if (TotalHappyness > ChangeFaces[2].y && TotalHappyness < ChangeFaces[2].x)
            {
                HappynessMeterChangeFaces.GetComponent<Image>().sprite = Resources.Load<Sprite>("SadHappyAnim_0035");
            }
            else if (TotalHappyness > ChangeFaces[3].y && TotalHappyness < ChangeFaces[3].x)
            {
                HappynessMeterChangeFaces.GetComponent<Image>().sprite = Resources.Load<Sprite>("SadHappyAnim_0050");
            }
            else if (TotalHappyness > ChangeFaces[4].y && TotalHappyness < ChangeFaces[4].x)
            {
                HappynessMeterChangeFaces.GetComponent<Image>().sprite = Resources.Load<Sprite>("SadHappyAnim_0065");
            }
            else if (TotalHappyness > ChangeFaces[5].y && TotalHappyness < ChangeFaces[5].x)
            {
                HappynessMeterChangeFaces.GetComponent<Image>().sprite = Resources.Load<Sprite>("SadHappyAnim_0080");
            }
            else if (TotalHappyness > ChangeFaces[6].y && TotalHappyness < ChangeFaces[6].x)
            {
                HappynessMeterChangeFaces.GetComponent<Image>().sprite = Resources.Load<Sprite>("SadHappyAnim_0100");
            }
            #endregion

            if (GameTimeInSeconds > 0) //if game is not over
            {
                //Game time printed in screen UI
                GameTimeUI.text = "Time Left : " + GameTimeInSeconds.ToString("000");

                #region Left and right customer allocation buttons interactability
                if (waitingQueuePositionList.Count != 0)
                {
                    if (waitingQueuePositionList[0].GetComponent<Customer1>().customerType == "Vital")
                    {
                        LeftButton.SetActive(false);
                        RightButton.SetActive(false);

                        if (waitingQueuePositionList[0].GetComponent<Customer1>().SecondColleauge)
                        {
                            SecondColleagueInfoBox.SetActive(true);
                        }
                        else
                        {
                            SecondColleagueInfoBox.SetActive(false);
                        }
                    }
                    else
                    {
                        LeftButton.SetActive(true);
                        RightButton.SetActive(true);
                        SecondColleagueInfoBox.SetActive(false);
                    }

                    if (waitingQueuePositionList[0].GetComponent<Customer1>().customerType == "Vital")
                    {
                        LeftButton.GetComponent<Button>().interactable = false;
                        RightButton.GetComponent<Button>().interactable = false;
                    }
                    else
                    {
                        if (waitingQueuePositionList[0].GetComponent<NavMeshAgent>().velocity != Vector3.zero)
                        {
                            LeftButton.GetComponent<Button>().interactable = false;
                            RightButton.GetComponent<Button>().interactable = false;
                        }
                        else
                        {
                            RightButton.GetComponent<Button>().interactable = true;
                            LeftButton.GetComponent<Button>().interactable = true;
                        }
                    }
                }
                else
                {
                    LeftButton.GetComponent<Button>().interactable = false;
                    RightButton.GetComponent<Button>().interactable = false;
                }
                #endregion

                #region Support Hide/Unhide character
                //Rest of the support system code is in SupportSystem.cs script which is attached to the SupportSystemCode gameobject in hierarchy.
                //The first spawn point shouldnt change at the moment. Otherwise system will not work (OR IF IT CHANGES VECTOR3 VALUE NEED CHANGING ACCORDINGLY)

                //vector3 coordinate is the original position of the support guy
                if (SupGOParent.transform.GetChild(0).localPosition == SupportPersonMainLocation)
                {
                    //SupGOParent.transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = false;
                    SupGOParent.transform.GetChild(0).GetComponentInChildren<SpriteRenderer>().enabled = false;
                }
                else
                {
                    //SupGOParent.transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = true;
                    SupGOParent.transform.GetChild(0).GetComponentInChildren<SpriteRenderer>().enabled = true;

                }

            #endregion

                #region Busy and Quiet Cycle System With Times
            //Day cycle system - Busy or quiet
            if (currentTimeOfTheDay >= 10 && currentTimeOfTheDay < 12)
                {
                    day = DayState.Quiet;
                }
                else if (currentTimeOfTheDay >= 12 && currentTimeOfTheDay < 14)
                {
                    day = DayState.Busy;
                }
                else if (currentTimeOfTheDay >= 14 && currentTimeOfTheDay < 16)
                {
                    day = DayState.Quiet;
                }
                else if (currentTimeOfTheDay >= 16 && currentTimeOfTheDay < 18.5f)
                {
                    day = DayState.Busy;
                }
                else if (currentTimeOfTheDay >= 18.5f)
                {
                    day = DayState.Quiet;
                }

                #endregion

                #region Depending of the day time send the correct customer
                if (!GamePause)
                {
                    int timeSlotRow = 0;
                    switch (day)
                    {
                        case DayState.Quiet:
                            timeSlotRow = 0;

                            if (targetLocations.Count != waitingQueuePositionList.Count)
                            {
                                StartCoroutine(RelocateTheCustomers(NonVitalCustomers, timeSlotRow));
                            }

                            break;

                        case DayState.Busy:
                            timeSlotRow = 1;

                            if (targetLocations.Count != waitingQueuePositionList.Count)
                            {
                                StartCoroutine(RelocateTheCustomers(NonVitalCustomers, timeSlotRow));
                            }
                            break;
                    }
                }
                #endregion

                #region Minus Point for each 3rd 4th 5th customer as punisment
                if (waitingQueuePositionList.Count > 2)
                {
                    PunishmentTimerCountDown -= 1 * Time.deltaTime;
                }
                if (waitingQueuePositionList.Count > 2 && waitingQueuePositionList.Count < 4)
                {
                    MinusPointPunishment(0);
                }
                else if (waitingQueuePositionList.Count > 3 && waitingQueuePositionList.Count < 5)
                {
                    MinusPointPunishment(1);
                }
                else if (waitingQueuePositionList.Count > 4 && waitingQueuePositionList.Count < 6)
                {
                    MinusPointPunishment(2);
                }
                #endregion
            }
            else
            {
                GameTimeUI.text = "Time Left : 0";
            }

            //Welcome label for the begining of the game
            if (welcomeUI2.activeInHierarchy)
            {
                welcomeUI2.SetActive(false);
            }
        }
        else
        {
            #region Question result screen
            if (CheckTheParentOfDismissMessage.activeInHierarchy)
            {
                countDownForTapToDismiss -= 1 * Time.deltaTime;
            }
            else
            {
                countDownForTapToDismiss = QuestionScreenDissapearTime;
            }
            if (countDownForTapToDismiss <= 0 && !TapToDismiss.activeInHierarchy)
            {
                TapToDismiss.SetActive(true);
                countDownForTapToDismiss = QuestionScreenDissapearTime;
            }
            if (TapToDismiss.activeInHierarchy)
            {
                //TapToDismiss.GetComponent<Animator>().SetTrigger("Flash");
                // JY NOTE generating a warning "Parameter 'Flash' does not exist"
            }
            #endregion

            #region Clock/First and second colleague animation

            Clock.GetComponent<Animator>().speed = 0;

            //Debug.Log("Anim: set speed 0");
            FirstEmployeeAnim.speed = 0;
            CoopFirstEmployee.transform.GetChild(0).GetComponent<Animator>().speed = 0;

            SecondColleague.GetComponent<Animator>().speed = 0;
            SecondColleague.transform.GetChild(0).GetComponent<Animator>().speed = 0;
            #endregion

            #region ANDROID OR IOS Touch controls with WelcomeScreen
#if UNITY_ANDROID || UNITY_IOS

            foreach (Touch t in Input.touches)
            {
                if (t.tapCount < 2)
                {
                    if (t.phase == TouchPhase.Began)
                    {
                        if (welcomeUI2.activeInHierarchy && GameTimeInSeconds > (TotalGameTime - 10))
                        {
                            countTouches++;

                            if (countTouches == 0)
                            {
                                welcomeUI2.SetActive(true);
                                WelcomeScreenImages[0].SetActive(true);
                            }
                            else if (countTouches == 1)
                            {
                                WelcomeScreenImages[0].SetActive(false);
                                WelcomeScreenImages[1].SetActive(true);
                            }
                            else if (countTouches == 2)
                            {
                                WelcomeScreenImages[1].SetActive(false);
                                WelcomeScreenImages[2].SetActive(true);

                            }
                            else if (countTouches == 3)
                            {
                                WelcomeScreenImages[2].SetActive(false);
                                WelcomeScreenImages[3].SetActive(true);

                            }
                            else if (countTouches == 4)
                            {
                                welcomeUI2.SetActive(false);
                                GamePause = false;
                                countTouches = 0;
                            }
                        }
                    }
                }
            }
#endif
            #endregion

            #region EDITOR OR STANDALONE Mouse click control system. Also welcome screen controls


#if UNITY_EDITOR || UNITY_STANDALONE
            if (Input.GetMouseButtonDown(0) &&
                welcomeUI2.activeInHierarchy &&
                GameTimeInSeconds > (TotalGameTime - 10))
            {
                countTouches++;

                if (countTouches == 0)
                {
                    welcomeUI2.SetActive(true);
                    WelcomeScreenImages[0].SetActive(true);
                }
                else if (countTouches == 1)
                {
                    WelcomeScreenImages[0].SetActive(false);
                    WelcomeScreenImages[1].SetActive(true);
                }
                else if (countTouches == 2)
                {
                    WelcomeScreenImages[1].SetActive(false);
                    WelcomeScreenImages[2].SetActive(true);

                }
                else if (countTouches == 3)
                {
                    WelcomeScreenImages[2].SetActive(false);
                    WelcomeScreenImages[3].SetActive(true);

                }
                else if (countTouches == 4)
                {
                    welcomeUI2.SetActive(false);
                    GamePause = false;
                    countTouches = 0;
                }
            }
#endif
            #endregion
        }
        // Debug.Log(EventSystem.current.currentSelectedGameObject.name + " selected right now");
    }

    public void MinusPointPunishment(int listNo)
    {
        if (PunishmentTimerCountDown <= 0)
        {
            HappynessMeterChangeFaces.GetComponent<Animator>().SetTrigger("HappySliderBounce");

            int minuesV = PointSystem.Instance.RemovePointFor3PeopleInQueue[listNo];
            // Debug.Log(minuesV + " taken point is this");
            MinusHappyness.GetComponent<Animator>().SetTrigger("isMinusRed");
            HappynessMeterAddOrRemove(minuesV);
            PunishmentTimerCountDown = timerSave;
        }
    }

    public void Left()//kiosk
    {
        if (!waitingQueuePositionList[0].GetComponent<Customer1>().SecondColleauge && 
            !SecondColleagueInUse && 
            !support)
        {
            //check the the first employees position. If its the original waiting position then make it move.
            Vector3 localPosition = CoopFirstEmployee.GetComponent<RectTransform>().localPosition;
            if (localPosition == new Vector3(50f, -42.4f, 0f))
            {
                FirstEmployeeAnim.enabled = true;
                FirstEmployeeAnim.SetBool("KioskWalk", true);

                Animator anim2 = CoopFirstEmployee.transform.GetChild(0).GetComponent<Animator>();
                anim2.SetBool("isWalking", true);
            }
            else
            {
                //Debug.Log("Anim: left() not at position");
                AnimatorStateInfo stateInfo = FirstEmployeeAnim.GetCurrentAnimatorStateInfo(0);
                if (stateInfo.IsName("WaitMovement"))
                {
                    //Debug.Log("Anim - is WaitMovement @ " + localPosition);
                    if (localPosition == new Vector3(50.3f, 19.3f, 0f))
                    {
                        // Added this test to prevent teleporting from behind counter to in front of it
                    }
                    else
                    {
                        FirstEmployeeAnim.SetBool("ReverseNow", true);
                        FirstEmployeeAnim.SetBool("KioskWalk", true);
                        //FirstEmployeeAnim.Play("WaitMovement", 1, 0);
                        FirstEmployeeAnim.Play("WaitMovement", -1, 0);
                        // JY NOTE the layer 1 is generating a warning "Invalid layer Index '1'"
                    }
                }
                if (stateInfo.IsName("FirstColleagueMovementAnimationBack"))
                {
                    //Debug.Log("Anim - is FirstColleagueMovementAnimationBack");
                    float xt = FirstEmployeeAnim.GetCurrentAnimatorStateInfo(0).normalizedTime;
                    float result = 1 - xt;
                    FirstEmployeeAnim.SetBool("ReverseNow", true);

                    FirstEmployeeAnim.Play("FirstColleagueMovementAnimationBack 0", -1, result);
                    FirstEmployeeAnim.SetBool("KioskWalk", true);
                }
            }
        }
        left = waitingQueuePositionList[0];
        CustomerInformation.Instance.AnyButtonPress = left;
    }

    public void Right()//ast
    {
        right = waitingQueuePositionList[0];
        CustomerInformation.Instance.AnyButtonPress = right;
    }

    public void WhichTillToGo(string tillName)
    {
        string whichTill = waitingQueuePositionList[0].GetComponent<Customer1>().WhereToGo;

        //set the speed of the customer for walking to the till
        if (whichTill == tillName)
        {
            if (NautralAnswer)
            {
                //Customer mood if they are happy or not with green or red texture
                waitingQueuePositionList[0].gameObject.transform.GetChild(0).GetChild(0).GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Icon_FaceYellowIndifferrent");
                SpriteRenderer render = waitingQueuePositionList[0].gameObject.transform.GetChild(0).GetChild(0).GetComponent<SpriteRenderer>();
                StartCoroutine(MakeColorDissapear(render));
            }
            else
            {
                HappynessMeterChangeFaces.GetComponent<Animator>().SetTrigger("HappySliderBounce");

                PlusHappyness.GetComponent<Animator>().SetTrigger("isPlusAdded");
                HappynessMeterAddOrRemove(PointSystem.Instance.AddPointForVitalAndNonVital); //10 for default
                //Customer mood if they are happy or not with green or red texture
                waitingQueuePositionList[0].gameObject.transform.GetChild(0).GetChild(0).GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Icon_FaceGreenHappy");
                SpriteRenderer render = waitingQueuePositionList[0].gameObject.transform.GetChild(0).GetChild(0).GetComponent<SpriteRenderer>();
                StartCoroutine(MakeColorDissapear(render));
            }

            //Speed of the customer after the decision is made
            waitingQueuePositionList[0].GetComponent<NavMeshAgent>().speed = CorrectTillCustomerSpeed;

            //add remove point
            TotalCorrectTill++;
        }
        else
        {
            if (waitingQueuePositionList[0].GetComponent<Customer1>().customerType != "NonVitalCustomers")//if its vital customer
            {
                if (NautralAnswer)
                {
                    //Customer mood if they are happy or not with green or red texture
                    waitingQueuePositionList[0].gameObject.transform.GetChild(0).GetChild(0).GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Icon_FaceYellowIndifferrent");
                    SpriteRenderer render = waitingQueuePositionList[0].gameObject.transform.GetChild(0).GetChild(0).GetComponent<SpriteRenderer>();
                    StartCoroutine(MakeColorDissapear(render));
                }
                else
                {
                    HappynessMeterChangeFaces.GetComponent<Animator>().SetTrigger("HappySliderBounce");

                    MinusHappyness.GetComponent<Animator>().SetTrigger("isMinusRed");

                    if (waitingQueuePositionList[0].GetComponent<Customer1>().MassiveImpact)
                    {
                        HappynessMeterAddOrRemove(PointSystem.Instance.RemovePointVitalCustomerMassiveImpact);
                    }
                    else
                    {
                        HappynessMeterAddOrRemove(PointSystem.Instance.RemovePointFromVital);
                    }
                    //Customer mood if they are happy or not with green or red texture
                    waitingQueuePositionList[0].gameObject.transform.GetChild(0).GetChild(0).GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Icon_FaceRedSad");
                    SpriteRenderer render = waitingQueuePositionList[0].gameObject.transform.GetChild(0).GetChild(0).GetComponent<SpriteRenderer>();
                    StartCoroutine(MakeColorDissapear(render));
                }
            }
            else if (waitingQueuePositionList[0].GetComponent<Customer1>().customerType == "NonVitalCustomers" &&
                    waitingQueuePositionList[0].GetComponent<Customer1>().customerNo == 111)
            {
                waitingQueuePositionList[0].gameObject.transform.GetChild(0).GetChild(0).GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Icon_FaceRedSad");
                HappynessMeterChangeFaces.GetComponent<Animator>().SetTrigger("HappySliderBounce");


                MinusHappyness.GetComponent<Animator>().SetTrigger("isMinusRed");
                HappynessMeterAddOrRemove(PointSystem.Instance.RemovePointNonVitalWrongAnswer);
                SpriteRenderer render = waitingQueuePositionList[0].gameObject.transform.GetChild(0).GetChild(0).GetComponent<SpriteRenderer>();
                StartCoroutine(MakeColorDissapear(render));
            }
            else
            {
                waitingQueuePositionList[0].gameObject.transform.GetChild(0).GetChild(0).GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Icon_FaceYellowIndifferrent");
                SpriteRenderer render = waitingQueuePositionList[0].gameObject.transform.GetChild(0).GetChild(0).GetComponent<SpriteRenderer>();
                StartCoroutine(MakeColorDissapear(render));
            }

            //Speed of the customer after the decision is made

            waitingQueuePositionList[0].GetComponent<NavMeshAgent>().speed = FalseTillCustomerSpeed;

            TotalWrongTill++;
        }
    }

    IEnumerator MakeColorDissapear(SpriteRenderer render)
    {
        yield return new WaitForSeconds(3);
        NautralAnswer = false;
        render.sprite = null;
    }

    public void Support()
    {
        support = true;
        TotalNumberOfSupoortButtonPresses++;
        SupportButton.GetComponent<Image>().fillAmount = 0f;

        Color tmp = SupGOParent.transform.GetChild(0).GetChild(0).GetComponent<SpriteRenderer>().color;
        tmp.a = 255f;
        SupGOParent.transform.GetChild(0).GetChild(0).GetComponent<SpriteRenderer>().color = tmp;

        if (CoopFirstEmployee.GetComponent<RectTransform>().position != new Vector3(78, 37.82f, -100.06f))
        {
            // JY NOTE: neither isGoingBack nor isRunningAround exist
            //CM_Handler.Instance.CoopFirstEmployee.GetComponent<Animator>().SetTrigger("isGoingBack");
            //CM_Handler.Instance.CoopFirstEmployee.GetComponent<Animator>().SetBool("isRunningAround", false);
            Debug.Log("Should we send the BakerGuy away from the kiosk here?");
            SendBakerGuyOut();

            CoopFirstEmployee.transform.GetChild(0).GetComponent<Animator>().SetBool("isWalking", true);
        }
    }

    void SupportMate(GameObject customerGO)//Second colleague gets activated
    {
        supportMateCustomerGO = customerGO;
        SecondColleagueInUse = true;
    }

    IEnumerator SetLeftRightBoolToFalse()
    {
        yield return new WaitForSeconds(.5f);
        //Debug.Log("Nullifying left and right");
        left = null;
        right = null;
    }

    IEnumerator RelocateTheCustomers(List<GameObject> daytime, int timeSlot)
    {
        yield return new WaitForSeconds(.1f);
        float Difference = TotalGameTime - GameTimeInSeconds;
        float divideValue = TotalGameTime / TotalHoursTheShopOpen;
        float currentTimeOfTheDay = (Difference / divideValue) + ShopOpeningTime;

        if (daytime.Count != 0)
        {
            customerSpawnTimer -= 1f * Time.deltaTime;

            if (customerSpawnTimer <= 0.1f)
            {
              ///  Debug.Log("Customer is called Now!!!!!!!!!!!!!!");
                CallTheCustomer(daytime);

                if (currentTimeOfTheDay < 7.5f)
                {
                    customerSpawnTimer = 5f;
                }
                else
                {
                    customerSpawnTimer = customerSpawningTimes[timeSlot];
                }
            }
        }
        else if (daytime == NonVitalCustomers && NonVitalCustomers.Count == 0)
        {
            RespawnNonVitalCustomers();
        }

     //   Debug.Log(customerSpawnTimer + " Spawn Time");

    }

    void RespawnNonVitalCustomers()
    {
        if (!RecycleSpawning)
        {
            RecycleSpawning = true;
            Transform nvtransform = nonvitalGo.transform;

            // Customers who are still in the shop
            List<GameObject> existingNonVitals = new List<GameObject>();

            for (int i = 0; i < nvtransform.childCount; i++)
            {
                existingNonVitals.Add(nvtransform.GetChild(i).gameObject);
            }

            AddCustomer.Instance.CreateNewCustomer();       // All the new ones get added to nonvitalGo
 
            // All the new ones need to go into the NonVitalCustomers list, but not the ones still in the shop
            for (int i = 0; i < nonvitalGo.transform.childCount; i++)
            {
                GameObject ngo = nvtransform.GetChild(i).gameObject;
                if (!existingNonVitals.Contains(ngo))
                {
                    NonVitalCustomers.Add(ngo);
                }
            }
            RecycleSpawning = false;
        }
    }
 
    //where we spawn a non vital or vital customers
    public void CallTheCustomer(List<GameObject> dayTime)
    {
        float Difference = TotalGameTime - GameTimeInSeconds;
        float divideValue = TotalGameTime / TotalHoursTheShopOpen;
        float currentTimeOfTheDay = (Difference / divideValue) + ShopOpeningTime;

        int i = Random.Range(0, dayTime.Count);//all customer random - Nonvitals

       // Debug.Log(i + " which customer is selected");
        //int b = Random.Range(3, 6);//Vital customer random

        int c = Random.Range(0, 2);// 0 is Non vital and 1 is vital

        int ForBuilderRandom = Random.Range(0, 3);

        if (!VitalIsSpawned)
        {
            if (!firstTime)
            {
                VitalOrNonVital = 0;
                firstTime = true;
            }
            else
            {
                if (NonVitalCustomerCounter < 3)
                {
                    VitalOrNonVital = ForBuilderRandom;
                }
                else
                {
                    VitalOrNonVital = c;
                }

                if (NonVitalCustomerCounter > 5 && c != 1)
                {
                    VitalOrNonVital = 1;
                }
            }
        }
        else
        {
            VitalOrNonVital = 0;

            if (NonVitalCustomerCounter > 3)
            {
                VitalIsSpawned = false;
            }
        }

       // Debug.Log(VitalOrNonVital + "Vital or non vital //////////////////////////////////////////");

        if (VitalOrNonVital != 1)//if its non vital customer
        {
          //  Debug.Log(i + " non vital customer order no *****************************************");

            // Check and set state
            Customer1 script = dayTime[i].GetComponent<Customer1>();

            if (script.customerNo == 777 && currentTimeOfTheDay > 20f)
            {
                if (dayTime.Count > 1)
                {
                    Destroy(dayTime[i]);
                }
                //fail safe for i
                if (NonVitalCustomers.Count == 0)
                {
                    RespawnNonVitalCustomers();
                }
                else
                {
                    if (i != 0)
                    {
                        i = (i - 1);
                    }
                }
                script = dayTime[i].GetComponent<Customer1>();
            }


            
            //Debug.Log("Spawn " + dayTime[i].name + ", " + script.customerName + " @ " + script.state);
           

            if (dayTime[i].transform.position != spawnPoint.transform.position)
            {
                // Creation of agent is delayed to here to get rid of the 'not close enough to the Navmesh' warning
                dayTime[i].transform.position = spawnPoint.transform.position;
                script.AddAgent();

                if (dayTime[i].GetComponent<NavMeshAgent>())
                {
                    NavMeshAgent agent = dayTime[i].GetComponent<NavMeshAgent>();
                    agent.Warp(spawnPoint.transform.position);
                }
                else
                {
                    script.AddAgent();
                    NavMeshAgent agent = dayTime[i].GetComponent<NavMeshAgent>();
                    agent.Warp(spawnPoint.transform.position);
                }
                Debug.Assert(script.state == State.Pool);
                script.state = State.Spawned;
                NonVitalCustomerCounter++;

                dayTime.RemoveAt(i);

             //   Debug.Log("Spawned number! " + i);
            }
        }
        else if (VitalOrNonVital == 1)
        {

            #region VitalCustomerSpawnArea
            VitalIsSpawned = true;
            NonVitalCustomerCounter = 0;

            if (currentTimeOfTheDay == 7.01f || currentTimeOfTheDay > 7.01f && currentTimeOfTheDay < 7.5f)
            {
                SpawnVital(0, 0);
            }
            if (currentTimeOfTheDay == 7.5f || currentTimeOfTheDay > 7.5f && currentTimeOfTheDay < 8f)
            {
                SpawnVital(1, 0);
            }
            if (currentTimeOfTheDay == 8f || currentTimeOfTheDay > 8f && currentTimeOfTheDay < 9f)
            {
                SpawnVital(2, 0);
            }
            if (currentTimeOfTheDay == 9f || currentTimeOfTheDay > 9f && currentTimeOfTheDay < 10f)
            {
                SpawnVital(3, 0);
            }
            if (currentTimeOfTheDay == 10f || currentTimeOfTheDay > 10f && currentTimeOfTheDay < 11.5f)
            {
                SpawnVital(4, 0);
            }
            if (currentTimeOfTheDay == 11.5f || currentTimeOfTheDay > 11.5f && currentTimeOfTheDay < 12f)
            {
                SpawnVital(5, 0);
            }
            if (currentTimeOfTheDay == 12f || currentTimeOfTheDay > 12f && currentTimeOfTheDay < 13f)
            {
                int xValue = Random.Range(0, 3);

                SpawnVital(6 + xValue, 0);
            }
            if (currentTimeOfTheDay == 13f || currentTimeOfTheDay > 13f && currentTimeOfTheDay < 15f)
            {
                SpawnVital(9, 0);
            }
            if (currentTimeOfTheDay == 15f || currentTimeOfTheDay > 15f && currentTimeOfTheDay < 16f)
            {
                SpawnVital(10, 0);
            }
            if (currentTimeOfTheDay == 16f || currentTimeOfTheDay > 16f && currentTimeOfTheDay < 17f)
            {
                if (Vitals.transform.GetChild(11).childCount > 1)
                {
                    int selectOneRandomly = Random.Range(0, 2);

                    SpawnVital(11, selectOneRandomly);
                }
            }
            if (currentTimeOfTheDay == 17f || currentTimeOfTheDay > 17f && currentTimeOfTheDay < 17.5f)
            {
                SpawnVital(12, 0);
            }
            if (currentTimeOfTheDay == 17.5f || currentTimeOfTheDay > 17.5f && currentTimeOfTheDay < 18f)
            {
                SpawnVital(13, 0);
            }
            if (currentTimeOfTheDay == 18f || currentTimeOfTheDay > 18f && currentTimeOfTheDay < 19f)
            {
                SpawnVital(14, 0);
            }
            if (currentTimeOfTheDay == 19f || currentTimeOfTheDay > 19f && currentTimeOfTheDay < 21f)
            {
                int xValue = Random.Range(0, 2);

                SpawnVital(15 + xValue, 0);
            }
            if (currentTimeOfTheDay == 21f || currentTimeOfTheDay > 21f && currentTimeOfTheDay < 22f)
            {
                if (Vitals.transform.GetChild(17).childCount > 1)//choses from 2 customer
                {
                    int selectOneRandomly = Random.Range(0, 2);
                    SpawnVital(17, selectOneRandomly);

                    if (OneHourRemaining.activeInHierarchy)
                    {
                        OneHourRemaining.SetActive(false);
                    }
                }
            }
            #endregion
        }
    }

    private void SpawnVital(int c1, int c2)
    {
        Customer1 script = Vitals.transform.GetChild(c1).GetChild(c2).GetComponent<Customer1>();
        //Debug.Log("Spawn " + script.name + ", " + script.customerName + " @ " + script.state);
        Debug.Assert(script.state == State.Pool);
        script.state = State.Spawned;
        script.transform.position = spawnPoint.transform.position;
        script.AddAgent();
        NavMeshAgent agent = Vitals.transform.GetChild(c1).GetChild(c2).GetComponent<NavMeshAgent>();
        agent.Warp(spawnPoint.transform.position);

        //send the second colleague if the customer requeire one
        if (script.SecondColleauge)
        {
            if (!support)
            {
                SupportMate(agent.gameObject);
            }
        }
    }

    public void Restart()
    {
        int counter = DontDestroy.Instance.gameSessionTime;//PlayerPrefs.GetInt("VideoSceneCounter");

        counter++;

        DontDestroy.Instance.gameSessionTime = counter;

        PlayerPrefs.SetInt("TotalCustomerServed", TotalCustomerServed);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void MainMenu()
    {
        int counter = DontDestroy.Instance.gameSessionTime;//PlayerPrefs.GetInt("VideoSceneCounter");

        counter++;

        DontDestroy.Instance.gameSessionTime = counter;

        SceneManager.LoadScene("MainMenu");
    }

    public void SendDataToJson()
    {
        int customerServed = TotalCustomerServed;
        int SupButtonPressed = TotalNumberOfSupoortButtonPresses;
        int corrctAnswers = TotalCorrectAnswers;
        int wrongAns = TotalWrongAnswers;
        int naturalAns = TotalNaturalAnswers;
        int correctTill = TotalCorrectTill;
        int wrongTill = TotalWrongTill;

        float happy = TotalHappyness;
        string date = System.DateTime.Now.ToString();//as unique id
        DontDestroy.Instance.CreateNewJson(date, corrctAnswers, wrongAns, naturalAns, customerServed, happy, SupButtonPressed, correctTill, wrongTill);
    }

    public void ARSButtonPress()
    {
        HappynessMeterChangeFaces.GetComponent<Animator>().SetTrigger("HappySliderBounce");

        PlusHappyness.GetComponent<Animator>().SetTrigger("isPlusAdded");
        HappynessMeterAddOrRemove(PointSystem.Instance.AddPointForAgeButtonPressed);

        HowManyARSCustomer.RemoveAt(0);

       // ARS.GetComponent<Animator>().SetBool("isActivatedNow", false);
        ARSButtonPressed = true;

        pointTaken = false;
    }

    public void PauseState()
    {
        PauseScreenPanel.SetActive(true);

        if (QuestionManager.Instance.PrefabParent.transform.childCount != 0)//if there is a vital customer and the game is already in pause mode
        {

        }
        else
        {
            GamePause = true;
        }
    }
   
    public void Resume()
    {
        PauseScreenPanel.SetActive(false);

        if (QuestionManager.Instance.PrefabParent.transform.childCount != 0)//if there is a vital customer and the game is already in pause mode
        {

        }
        else
        {
            GamePause = false;
        }
    }

    public void TutorialButton()
    {
        TutorialButtonClicked = true;
        Debug.Log(TutorialButtonClicked);
    }

    public void SendBakerGuyOut()
    {
        //
        //Debug.Log("Send the BakerGuy away from the kiosk");
        FirstEmployeeAnim.SetBool("ReverseNow", false);
        FirstEmployeeAnim.SetBool("KioskWalk", false);
    }
}