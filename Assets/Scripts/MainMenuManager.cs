﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviour
{
    public GameObject Loading;
    public GameObject Game;
    public GameObject Images;
    public GameObject Video;

    public GameObject GenericImage;
    public AudioSource ButtonClickSound;

    GameObject dontDestroy;

    private void Start()
    {
        Loading.SetActive(false);
        dontDestroy = GameObject.Find("DontDestroyObject");

        //In coroutine because scene loads slow and code cannot detect correctly if its guest login or normal login
        StartCoroutine(WaitForSystemtoLoad());
    }

    IEnumerator WaitForSystemtoLoad()
    {
        yield return new WaitForSeconds(.08f);
        if (dontDestroy.GetComponent<DontDestroy>().userIDValue != "123456")
        {
            GenericImage.GetComponent<Image>().sprite = Resources.Load<Sprite>("MainMenu/Screen_Options_2");
        }
        else
        {
            GenericImage.GetComponent<Image>().sprite = Resources.Load<Sprite>("MainMenu/Screen_Options_6");
        }
    }

    public void PlayAudio()
    {
        if (Images.GetComponent<Button>().interactable)
        {
            ButtonClickSound.Play();
        }
    }

    void Update()
    {
        if (dontDestroy.GetComponent<DontDestroy>().userIDValue == "123456")
        {
            Images.GetComponent<Button>().interactable = false;
            Game.GetComponent<Button>().interactable = false;
        }
    }

    //Start the game
    public void LoadGame()
    {
        SceneManager.LoadScene("Game");
    }

    public void ImageScene()
    {
        SceneManager.LoadScene("ImageScene");
    }

    public void DocumentScene()
    {
        SceneManager.LoadScene("DocumentationScene");
    }

    public void VideoScene()
    {
        SceneManager.LoadScene("VideoScene");
    }
    public void SaveToJson()
    {
        DontDestroy.Instance.Click();
    }

    public void LoadingScreen()
    {
        Loading.SetActive(true);
    }
}
