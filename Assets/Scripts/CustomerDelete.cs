﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class CustomerDelete : MonoBehaviour
{

    void OnTriggerEnter(Collider col)
    {
       Destroy(col.gameObject);
    }
}
