﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

[RequireComponent(typeof(BoxCollider))]
public class CountForPayment : MonoBehaviour
{
    public static CountForPayment Instance;
    public float paymentCount;
    public GameObject Exit;
    public string TypeOfThisTill;
    public GameObject ARS;

    Animator CoopEmployeeSwapMoveAnim, CoopEmployeeSwapWalkAnim;

    List<GameObject> JustEntered = new List<GameObject>();
    GameObject refGO;


    void Awake()
    {
        //isACustomerInTheTillArea = false;
        Instance = this;
        paymentCount = 0;
    }

    private void Start()
    {
        //animation for baker guy where s/he moves from one point to another
        CoopEmployeeSwapMoveAnim = CM_Handler.Instance.CoopFirstEmployee.GetComponent<Animator>();
        //this animation makes the baker guy shake while move
        CoopEmployeeSwapWalkAnim = CM_Handler.Instance.CoopFirstEmployee.transform.GetChild(0).GetComponent<Animator>();
        //SecondColleagueWalkAnim = CM_Handler.Instance.SecondColleague.transform.GetChild(0).GetComponent<Animator>();
        refGO = CM_Handler.Instance.CoopFirstEmployee;
    }

    void OnTriggerEnter(Collider col)
    {
        //Debug.Log("Enter CountForPayment " + col.name);
        col.GetComponentInChildren<SpriteRenderer>().sortingLayerName = "3";
       

        if (TypeOfThisTill == "Kiosk")
        {
            JustEntered.Add(col.gameObject);

            CM_Handler.Instance.UnavailableTill_Kiosk.Add(gameObject);//since customer is already on the queue add this till to unavailables list
            CM_Handler.Instance.AvailableTills_Kiosk.RemoveAt(0);// also remove the till from availables list
        }

        if (col.GetComponent<Customer1>().WhereToGo == TypeOfThisTill)
        {
            paymentCount = CM_Handler.Instance.CorrectTillWaitTime;//the time that customer spend on the till
        }
        else
        {
            paymentCount = CM_Handler.Instance.WrongTillWaitTime;
        }

        Customer1 script = col.gameObject.GetComponent<Customer1>();
        if (script.state == State.InKioskQueue)
            script.state = State.AtKiosk;
        else if (script.state == State.InAstQueue)
            script.state = State.AtAst;
        else
        {
            Debug.LogWarning(script.customerName + " arrived at till in state " + script.state);
        }


        if (TypeOfThisTill == "Kiosk")
        {
            //if first colleauge isnt in her waiting position and the next customer require the second colleauge then send the first colleague back
          
                if (col.gameObject.GetComponent<Customer1>().SecondColleauge &&
                    CM_Handler.Instance.CoopFirstEmployee.GetComponent<RectTransform>().position != new Vector3(78, 37.82f, -100.06f))
                {
                    // JY NOTE: neither isGoingBack nor isRunningAround exist
                    //CM_Handler.Instance.CoopFirstEmployee.GetComponent<Animator>().SetTrigger("isGoingBack");
                    //CM_Handler.Instance.CoopFirstEmployee.GetComponent<Animator>().SetBool("isRunningAround", false);
                    Debug.Log("Should we send the BakerGuy away from the kiosk here?");
                    CM_Handler.Instance.SendBakerGuyOut();

                    CoopEmployeeSwapWalkAnim.SetBool("isWalking", true);
                }
            }
        
    }

    void OnTriggerStay(Collider col)
    {
        //this is required so animation doesnt play during "game pause" times.
        bool isWalking = CoopEmployeeSwapWalkAnim.GetBool("isWalking");
        bool gamePause = CM_Handler.Instance.GamePause;
        if (isWalking && gamePause)
        {
            CoopEmployeeSwapWalkAnim.SetBool("isWalking", false);
        }
        //else if (!isWalking && !gamePause && !CoopEmployeeSwapMoveAnim.GetCurrentAnimatorStateInfo(0).IsName("WaitMovement"))
        // JY NOTE NullReferenceException
        else if (!isWalking && !gamePause && !CoopEmployeeSwapMoveAnim.GetCurrentAnimatorStateInfo(0).IsName("WaitMovement"))
        {
            CoopEmployeeSwapWalkAnim.SetBool("isWalking", true);
        }

        col.GetComponentInChildren<SpriteRenderer>().sortingLayerName = "3";
        if (paymentCount == 0)
        {
            col.GetComponent<NavMeshAgent>().destination = Exit.transform.position ;
            Customer1 script = col.gameObject.GetComponent<Customer1>();
            Debug.AssertFormat(script.state == State.AtKiosk || script.state == State.AtAst || script.state == State.Leaving, script.customerName + " had state " + script.state);
            // JY NOTE Once it's in Leaving state, might as well not process OnTriggerStay() at all
            script.state = State.Leaving;
        }

        if (CoopEmployeeSwapMoveAnim.GetCurrentAnimatorStateInfo(0).IsName("WaitMovement"))
        {
            CoopEmployeeSwapWalkAnim.SetBool("isWalking", false);
        }
        //if there is a customer in the queue but our first employee waiting in her default position make her move to the kiosk
        if (TypeOfThisTill == "Kiosk")
        {
            if (JustEntered.Count != 0)
            {
                if (!JustEntered[0].GetComponent<Customer1>().SecondColleauge && !CM_Handler.Instance.SecondColleagueInUse)
                {
                    if (refGO.GetComponent<RectTransform>().localPosition == new Vector3(50f, -42.4f, 0f))
                    {
                        if (!CM_Handler.Instance.support)
                        {
                            refGO.GetComponent<Animator>().enabled = true;
                            //Animator anim = refGO.GetComponent<Animator>();
                            // anim.SetBool("isRunningAround", true);
                            CM_Handler.Instance.CoopFirstEmployee.GetComponent<Animator>().SetBool("KioskWalk", true);

                            Animator anim2 = refGO.transform.GetChild(0).GetComponent<Animator>();
                            anim2.SetBool("isWalking", true);
                        }
                    }
                }
            }
        }
    }


    void OnTriggerExit(Collider col)
    {
        //Debug.Log("Exit CountForPayment " + col.name);
        Debug.Assert(col.gameObject.GetComponent<Customer1>().state == State.Leaving);

        col.GetComponentInChildren<SpriteRenderer>().sortingLayerName = "5";

        if (TypeOfThisTill == "AST")
        {
            StartCoroutine(WaitForSecondsToDelete());//we need to wait before we switch which till is availbale which till is not
        }
        else if (TypeOfThisTill == "Kiosk")
        {
            CM_Handler.Instance.AvailableTills_Kiosk.Add(gameObject);
            CM_Handler.Instance.UnavailableTill_Kiosk.Remove(gameObject);

            if (KioskQueueSystem.Instance.KioskWaitingQueueList.Count == 0 )
            {
                if (CM_Handler.Instance.CoopFirstEmployee.GetComponent<RectTransform>().localPosition != new Vector3(50f, -42.4f, 0f))
                {
                    StartCoroutine(WaitForTheCustomerToLeave());
                }
            }
        }      
        //What happens to the ARS button when customer left its till
        if (col.GetComponent<Customer1>().ARSButton)
        {
            if (CM_Handler.Instance.HowManyARSCustomer.Count != 0 && !CM_Handler.Instance.ARSButtonPressed)
            {
                if (col.gameObject == CM_Handler.Instance.HowManyARSCustomer[0])
                {
                    CM_Handler.Instance.HowManyARSCustomer.RemoveAt(0);
                    CM_Handler.Instance.ARSReadyToCheck = true;
                }
            }
        }

        //if we dont put this under the if statements for ast or kiosk it will happen in any till.
        //this is sending the second colleague texture to its original location by setting/detecting when the customer left its till
        if (CM_Handler.Instance.SecondColleagueInUse && col.GetComponent<Customer1>().customerType == "Vital")
        {
            CM_Handler.Instance.customerGoingHome = true;
        }

        if (JustEntered.Count != 0)
        {
            JustEntered.RemoveAt(0);

        }
    }
    IEnumerator WaitForTheCustomerToLeave()
    {
        yield return new WaitForSeconds(1.5f);

        if (JustEntered.Count == 0 && KioskQueueSystem.Instance.KioskWaitingQueueList.Count == 0 )
        {
            CM_Handler.Instance.CoopFirstEmployee.GetComponent<Animator>().SetBool("KioskWalk", false);

            CoopEmployeeSwapWalkAnim.SetBool("isWalking", true);
        }
    }
    IEnumerator WaitForSecondsToDelete()
    {
        yield return new WaitForSeconds(.5f);
        if (gameObject.name == "AST")
        {
            if (CM_Handler.Instance.AvailableTills_AST_1.Count < 1)
            {
                CM_Handler.Instance.AvailableTills_AST_1.Add(gameObject);
                CM_Handler.Instance.UnavailableTill_AST_1.Remove(gameObject);
            }
        }
        else if (gameObject.name == "AST2")
        {
            if (CM_Handler.Instance.AvailableTills_AST_2.Count < 1)
            {
                CM_Handler.Instance.AvailableTills_AST_2.Add(gameObject);
                CM_Handler.Instance.UnavailableTill_AST_2.Remove(gameObject);
            }

        }
    }
	// Update is called once per frame
	void Update ()
    {
        if (!CM_Handler.Instance.GamePause)
        {
            if(CM_Handler.Instance.support)
            {
                CM_Handler.Instance.CoopFirstEmployee.GetComponent<Animator>().SetBool("KioskWalk", false);
                CoopEmployeeSwapWalkAnim.SetBool("isWalking", true);
            }

            if (paymentCount != 0)
            {
                if (CM_Handler.Instance.fillSystemWork)
                {
                    paymentCount -= SupportSystem.Instance.TillSpeedWithBonus * Time.deltaTime;

                    if (paymentCount < 0)
                    {
                        paymentCount = 0;
                    }
                }
                else
                {
                    paymentCount -= 1 * Time.deltaTime;

                    if (paymentCount < 0)
                    {
                        paymentCount = 0;
                    }
                }
            }

            if (CoopEmployeeSwapMoveAnim.GetCurrentAnimatorStateInfo(0).IsName("ExtraCollegueAnimation"))
            {
                CoopEmployeeSwapWalkAnim.SetBool("isWalking", true);
            }
            if (CoopEmployeeSwapMoveAnim.GetCurrentAnimatorStateInfo(0).IsName("New State"))
            {
                if (CoopEmployeeSwapWalkAnim.GetBool("isWalking"))
                {
                    CoopEmployeeSwapWalkAnim.SetBool("isWalking", false);
                }
            }
        }
	}
}
