﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class layerSystem3 : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Second")
        {
            if (other.GetComponent<SpriteRenderer>().sortingLayerName != "9")
            {
                other.GetComponent<SpriteRenderer>().sortingLayerName = "9";
            }
        }
    }
}
