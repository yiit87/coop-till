﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class VideoSceneManager : MonoBehaviour
{
    [Header("Total Video Time In Seconds")]
    public double TimeOfTheVideo;

    [Header("UI Menu Panel")]
    public GameObject UIElements;
    public GameObject ButtonPanel;

    [Header("Video Player GameObject")]
    public GameObject VideoPlayerSystem;

    [Header("Button Itself")]
    public GameObject ResumeButtonUI;

    private VideoPlayer videoPlayer;

    private void Start()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        UIElements.SetActive(false);
        ResumeButtonUI.SetActive(true);

        videoPlayer = VideoPlayerSystem.GetComponent<VideoPlayer>();
        
        videoPlayer.SetTargetAudioSource(0, VideoPlayerSystem.GetComponent<AudioSource>());

     //   videoPlayer.source = VideoPlayerSystem.GetComponent<AudioSource>();
    }
   
    private void Update()
    {
        if (!UIElements.activeInHierarchy)
        {
           
#if !UNITY_EDITOR || UNITY_ANDROID || UNITY_IOS
            foreach (Touch t in Input.touches)
            {
                if (t.phase == TouchPhase.Began)
                {
                    videoPlayer.Pause();
                    UIElements.SetActive(true);
                }
            }
#endif
#if UNITY_EDITOR && UNITY_ANDROID
            if (Input.GetMouseButtonDown(0))
            {
                videoPlayer.Pause();
                StartCoroutine(DelayButtonShowups());
            }
#endif
            if (videoPlayer.time >= TimeOfTheVideo)
            {
                ResumeButtonUI.SetActive(false);
                videoPlayer.Pause();
                UIElements.SetActive(true);
            }
            Debug.Log(VideoPlayerSystem.GetComponent<VideoPlayer>().frame + " frame");
            Debug.Log(VideoPlayerSystem.GetComponent<VideoPlayer>().frameCount + " frame count");

            if (videoPlayer.isPlaying)
            {
                if (videoPlayer.frame == (long)videoPlayer.frameCount)
                {
                    SceneManager.LoadScene("MainMenu");
                }
            }
        }
    }
    IEnumerator DelayButtonShowups()
    {
        yield return new WaitForSeconds(.5f);
        UIElements.SetActive(true);
    }
    
    public void ResumeButton()
    {
        Debug.Log("Resume button pressed");
        videoPlayer.Play();
        UIElements.SetActive(false);
    }
    public void RestartButton()
    {
        int counter = DontDestroy.Instance.videoWatchTime;//PlayerPrefs.GetInt("VideoSceneCounter");

        counter++;
        DontDestroy.Instance.Click();
        videoPlayer.Stop();
        videoPlayer.Play();
        UIElements.SetActive(false);

       // PlayerPrefs.SetInt("VideoSceneCounter", counter);
    }
   
    public void BackToMainMenu()
    {
        int counter = DontDestroy.Instance.videoWatchTime;//PlayerPrefs.GetInt("VideoSceneCounter");

        counter++;

        DontDestroy.Instance.videoWatchTime = counter;

        DontDestroy.Instance.Click();
        videoPlayer.Stop();
        SceneManager.LoadScene("MainMenu");
    }
}
