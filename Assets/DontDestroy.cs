﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Statistics
{
    public string sessionsID;
    public int correctAnswers;
    public int wrongAnswers;
    public int neutralAnswers;
    public int totalCustomerServed;
    public float totalHappynessValue;
    public int totalSupportbuttonPresses;
    public int totalCorrectTill;
    public int totalWrongTill;
}
public class DontDestroy : MonoBehaviour {

    public static DontDestroy Instance;

    [Header("Statistics")]
    public string userIDValue;
    public int documentReadingTime;
    public int videoWatchTime;
    public int imageViewTime;
    public int gameSessionTime;
    string json;
    public List<Statistics> sessions = new List<Statistics>();

    // public string GetTheDataFromServer;

    [System.NonSerialized]
    public List<string> dividedData = new List<string>();

    private void Awake()
    {
        Instance = this;
    }
    // Use this for initialization
    void Start ()
    {
        DontDestroyOnLoad(gameObject);
    }

    public void CreateNewJson(string sessionID, int correctAnswer, int wrongAnswer, int naturelAnswer, int totalCustomerServed, float totalHappynessValue,int supButtonPresses, int totalCorrectTill, int totalWrongTill)
    {
        Statistics fillDetails = new Statistics();

        fillDetails.sessionsID = sessionID;
        fillDetails.correctAnswers = correctAnswer;
        fillDetails.wrongAnswers = wrongAnswer;
        fillDetails.neutralAnswers = naturelAnswer;
        fillDetails.totalCustomerServed = totalCustomerServed;
        fillDetails.totalHappynessValue = totalHappynessValue;
        fillDetails.totalSupportbuttonPresses = supButtonPresses;
        fillDetails.totalCorrectTill = totalCorrectTill;
        fillDetails.totalWrongTill = totalWrongTill;

        sessions.Add(fillDetails);

        Click();
    }
	public void LoginButton()
    {
        StartCoroutine(WaitDataToCome());
    }

    IEnumerator WaitDataToCome()
    {
        yield return new WaitForSeconds(1);

        /*if (InternetSubmission.Instance.GetTheData != null)
        {
            /*userIDValue = PlayerPrefs.GetString("UserID");
            documentReadingTime = PlayerPrefs.GetInt("DocumentationSceneCounter");
            videoWatchTime = PlayerPrefs.GetInt("VideoSceneCounter");
            imageViewTime = PlayerPrefs.GetInt("ImageSceneCounter");
            gameSessionTime = PlayerPrefs.GetInt("GameSessionCounter");*/
          /*  string replaceWords = InternetSubmission.Instance.GetTheData.Replace("OK^{", "").Replace("}", "").Replace("\"", "");
        
            string[] divide = replaceWords.Split(',');

            for (int i = 0; i < divide.Length; i++)
            {
                string[] lastDivide = divide[i].Split(':');

                for (int k = 0; k < lastDivide.Length; k++)
                {
                    dividedData.Add(lastDivide[k]);

                }
            }
            Debug.Log(dividedData[1]+ "/"+ dividedData[3] + "/"+dividedData[5]+"/"+ dividedData[7] + "/" + dividedData[9]);

            if (dividedData != null)
            {
                userIDValue = dividedData[1];

                if ( dividedData[3] == "0")
                {
                    documentReadingTime = 0;
                }else
                {
                    documentReadingTime =int.Parse(dividedData[3]);
                }

                videoWatchTime = int.Parse(dividedData[5]);
                imageViewTime = int.Parse(dividedData[7]);
            }
        }*/
    }

    public void Click()
    {
        string test = userIDValue + documentReadingTime.ToString() + videoWatchTime.ToString() + imageViewTime.ToString() + gameSessionTime.ToString();
        json = JsonUtility.ToJson(this);
        StartCoroutine(SendToDatabase());
        Debug.Log(json);
    }
    IEnumerator SendToDatabase()
    {
        string url = "http://clicksandlinks.com/coop/ygt/shoveii.php?x=" + WWW.EscapeURL(json) ;
        WWW post = new WWW(url);

        yield return post;
    }
}
