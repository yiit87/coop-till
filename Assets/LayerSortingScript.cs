﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayerSortingScript : MonoBehaviour {

    private void OnTriggerEnter (Collider other)
    {
        if (other.gameObject.name == "BakerGuy")
        {
            //if (other.GetComponent<SpriteRenderer>().sortingLayerName == "5")
            if (BakerGuyGoingToKiosk(other))
            {
                other.GetComponent<SpriteRenderer>().sortingLayerName = "1";
            }
            if (other.GetComponent<SpriteRenderer>().sortingLayerName == "1a")
            {
                other.GetComponent<SpriteRenderer>().sortingLayerName = "5";
            }
        
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "BakerGuy")
        {
            //if (other.GetComponent<SpriteRenderer>().sortingLayerName == "5")
            if (BakerGuyGoingToKiosk(other))
            {
                other.GetComponent<SpriteRenderer>().sortingLayerName = "1";
            }
            else
            {
                other.GetComponent<SpriteRenderer>().sortingLayerName = "5";
            }
        }
    }

    public static bool BakerGuyGoingToKioskGuyCheck(Collider other)
    {
        return other.gameObject.name == "BakerGuy" && BakerGuyGoingToKiosk(other);
    }

    public static bool BakerGuyGoingToKiosk(Collider other)
    {
        return !other.transform.parent.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("FirstColleagueMovementAnimationBack");
    }
}
