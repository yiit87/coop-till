﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class QuestionManager : MonoBehaviour {

    public static QuestionManager Instance;

    [Header("Parent Object Of Instantiated Obj")]
    public GameObject PrefabParent;

    [Header("Prefab Itself")]
    public GameObject QuestionPanelPrefab;

    string[] divide = new string[] { };
    public bool buttonPressed = false; //Answer buttons pressed or not
    bool destoryTheObjectNow = false; //Destroy the clone that is created with this bool

    [Header("Time to wait for accepting touches to the screen after an answer is selected")]
    public float WaitBeforeTouch;

    public GameObject ResultOfTheAnswer;
    public GameObject imageObject;
    public GameObject ResultText;

    [Header("Waiting Time Before Showing The Questions")]
    public float WaitingTimeQuestions;

    private float PressedTime;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        ResultOfTheAnswer.SetActive(false);
    }

    private void Update()
    {
            //code safety
    }

    // Update is called once per frame
    void OnTriggerStay(Collider col)
    {
        if (gameObject.name == "GameManager")
        {
            //if there is at least 1 customer in the queue
            if (CM_Handler.Instance.waitingQueuePositionList.Count != 0)
            {
                //If the first person in the queue is not moving  and customertype is Vital
                if (CM_Handler.Instance.waitingQueuePositionList[0].GetComponent<NavMeshAgent>().velocity == Vector3.zero &&
                    CM_Handler.Instance.waitingQueuePositionList[0].GetComponent<Customer1>().customerType == "Vital")//are they waiting or moving
                {
                    if (CM_Handler.Instance.waitingQueuePositionList[0].gameObject == col.gameObject)
                    {
                        //if kiosk till is available and there is at least 1 available AST till then get the questions
                        //We do this check in the IEnumerator for double check because sometimes its too fast and doesnt realize it

                        if (KioskQueueSystem.Instance.KioskWaitingQueueList.Count < 2 || ASTQueueSystem.Instance.ASTWaitingAreaList.Count < 2)
                        {
                            if (CM_Handler.Instance.AlreadyChosenOption())
                            {
                                //Debug.Log("We've already shown the questions for this customer");
                                // We also hit this when the result is displayed
                            }
                            else
                            {
                                StartCoroutine(InstantiateTheObject());
                            }
                        }
                    }
                }
                else
                {

                }
            }
        }
#if UNITY_ANDROID || UNITY_IOS
            if (destoryTheObjectNow)
            {
                foreach (Touch t in Input.touches)
                {
                    if (t.phase == TouchPhase.Began)
                    {
                    //if (PrefabParent.transform.childCount > 0)
                        if(CM_Handler.Instance.TapToDismiss.activeInHierarchy)
                        {
                            CM_Handler.Instance.TapToDismiss.SetActive(false);

                            Destroy(PrefabParent.transform.GetChild(0).gameObject);

                            PressedTime = Time.time;
                        }

                        CM_Handler.Instance.SecondColleagueInfoBox.SetActive(false);

                        //NOT AN IDEAL WAY OF DOING THIS BUT IT FIXED THE PROBLEM FOR THE TIME BEING
                        if (CM_Handler.Instance.CoopFirstEmployee.GetComponent<RectTransform>().position != new Vector3(78, 37.82f, -100.06f))
                        {
                            CM_Handler.Instance.CoopFirstEmployee.GetComponent<Animator>().enabled = true;
                        }

                        CM_Handler.Instance.GamePause = false;
                        destoryTheObjectNow = false;
                        ResultOfTheAnswer.SetActive(false);

                        StartCoroutine(SetButtonToFalse());
                    }
                }
            }
#endif
#if UNITY_EDITOR || UNITY_STANDALONE
        if (destoryTheObjectNow)
        {
            if (Input.GetMouseButtonDown(0))
            {
                //if (PrefabParent.transform.childCount > 0)
                if (CM_Handler.Instance.TapToDismiss.activeInHierarchy)
                {
                    CM_Handler.Instance.TapToDismiss.SetActive(false);

                    Destroy(PrefabParent.transform.GetChild(0).gameObject);
                }

                CM_Handler.Instance.SecondColleagueInfoBox.SetActive(false);

                //NOT AN IDEAL WAY OF DOING THIS BUT IT FIXED THE PROBLEM FOR THE TIME BEING
                if (CM_Handler.Instance.CoopFirstEmployee.GetComponent<RectTransform>().position != new Vector3(78,37.82f, -100.06f))
                {
                    CM_Handler.Instance.CoopFirstEmployee.GetComponent<Animator>().enabled = true;
                }

                CM_Handler.Instance.GamePause = false;
                destoryTheObjectNow = false;
                ResultOfTheAnswer.SetActive(false);

                StartCoroutine(SetButtonToFalse());
            }
        }
#endif
       // Debug.Log(destoryTheObjectNow + " bool condition here");
    }
    IEnumerator SetButtonToFalse()
    {
        yield return new WaitForSeconds(3);
        buttonPressed = false;
    }

    IEnumerator InstantiateTheObject()
    {
        yield return new WaitForSeconds(WaitingTimeQuestions);

        if (KioskQueueSystem.Instance.KioskWaitingQueueList.Count <3 && ASTQueueSystem.Instance.ASTWaitingAreaList.Count <3)
        {
            //did player pressed the button or not and if there is another child of the question panel
            if (!buttonPressed && PrefabParent.transform.childCount == 0)
            {
                if (CM_Handler.Instance.waitingQueuePositionList.Count != 0)
                {
                    Customer1 customer1 = CM_Handler.Instance.waitingQueuePositionList[0].GetComponent<Customer1>();

                    if (customer1.state == State.InFirstQueue)
                    {
                        if (CM_Handler.Instance.AlreadyChosenOption())
                        {
                            Debug.Log("THIS IS A PROBLEM");
                        }

                        string[] questions = customer1.customerVitalQuestions;

                        if (questions[0].Length != 3)
                        {
                            if (CM_Handler.Instance.OneHourRemaining.activeInHierarchy)
                            {
                                CM_Handler.Instance.OneHourRemaining.SetActive(false);
                            }
                            //Instantiate a game object
                            GameObject game = Instantiate(QuestionPanelPrefab, PrefabParent.transform);
                            game.SetActive(true);

                            //Pause the game
                            CM_Handler.Instance.GamePause = true;

                            //divide the question so we know which answer is correct
                            //for each child object put the correct informations and implement a listener
                            for (int i = 0; i < game.transform.childCount; i++)
                            {
                                divide = questions[i].Split('/');

                                game.transform.GetChild(i).transform.gameObject.name = divide[1];
                                game.transform.GetChild(i).transform.GetComponentInChildren<Text>().text = divide[0];

                                game.transform.GetChild(i).GetComponent<Button>().onClick.AddListener(SelectedAnswer);
                            }
                        }
                    }
                    else
                    {
                        // We've already dispatched this customer
                        Debug.Log("QUESTION ATTEMPT FOR CUSTOMER '" + customer1.customerName + "' IN WRONG STATE - state " + customer1.state);

                        if (Time.time - PressedTime > 10f)
                        {
                            customer1.state = State.InFirstQueue;
                        }
                    }
                }
            }
        }
    }
    //what happens when button is pressed
    public void SelectedAnswer()
    {
        GameObject mainPanel = PrefabParent.transform.GetChild(0).gameObject;

        //where normally customer supposed to go
        string where = CM_Handler.Instance.waitingQueuePositionList[0].GetComponent<Customer1>().WhereToGo;

        //Show the color of the button depending of the answer
        if (EventSystem.current.currentSelectedGameObject.name == "C")
        {
            CM_Handler.Instance.TotalCorrectAnswers++;
            //Determine where is the correct till for customer
            if (where == "AST")
            {
                CM_Handler.Instance.Right();
            }
            else
            {
                CM_Handler.Instance.Left();
            }

            StartCoroutine(HideTheSelected(EventSystem.current.currentSelectedGameObject, "AnswerSplash_Happy"));
        }
        else if (EventSystem.current.currentSelectedGameObject.name == "R")
        {
            //In case of the wrong answer
            CM_Handler.Instance.TotalWrongAnswers++;

            //send the customer to the opposite till (to the wrong one)
            if (where == "Kiosk")
            {
                CM_Handler.Instance.Right();
            }
            else
            {
                CM_Handler.Instance.Left();
            }
            StartCoroutine(HideTheSelected(EventSystem.current.currentSelectedGameObject, "AnswerSplash_Sad"));
        }
        else if (EventSystem.current.currentSelectedGameObject.name == "N")
        {
            //Answer is naturel
            CM_Handler.Instance.TotalNaturalAnswers++;
            CM_Handler.Instance.NautralAnswer = true;

            //TODO: Send the customer to the till that is written in the scenario.
            if (where == "AST")
            {
                CM_Handler.Instance.Left();
            }
            else
            {
                CM_Handler.Instance.Right();
            }

            StartCoroutine(HideTheSelected(EventSystem.current.currentSelectedGameObject, "AnswerSplash_Indifferrent"));
        }
        //Deactivate the buttons that arent selected
        for (int i = 0; i < mainPanel.transform.childCount; i++)
        {
            if(mainPanel.transform.GetChild(i).name != EventSystem.current.currentSelectedGameObject.name)
            {
                mainPanel.transform.GetChild(i).gameObject.SetActive(false);
            }
        }
        
        buttonPressed = true;

        StartCoroutine(ShowTheResult());
    }
    IEnumerator HideTheSelected(GameObject selected, string stringValue)
    {
        yield return new WaitForSeconds(.5f);

        selected.SetActive(false);

        imageObject.GetComponent<Image>().sprite = Resources.Load<Sprite>(stringValue);
        ResultOfTheAnswer.SetActive(true);
    }
    IEnumerator ShowTheResult()
    {
        //switch the text of the buttons to the result of the selected buttons answer.
        yield return new WaitForSeconds(.01f);

        try
        {
            string[] resultdivide = CM_Handler.Instance.waitingQueuePositionList[0].GetComponent<Customer1>().customerVitalResults;

            int buttonName = EventSystem.current.currentSelectedGameObject.gameObject.transform.GetSiblingIndex(); ;

            ResultText.GetComponent<Text>().text = resultdivide[buttonName];
        }
        catch
        {
            Debug.Log("Catch");
        }
        //after 3.5 seconds allow user to press/touch to the screen and destroy the clone gameobject
        yield return new WaitForSeconds(WaitBeforeTouch);
        StartCoroutine(CloseTheWindow());
    }

    IEnumerator CloseTheWindow()
    {
        yield return new WaitForSeconds(CM_Handler.Instance.QuestionScreenDissapearTime);

        destoryTheObjectNow = true;
    }
}
