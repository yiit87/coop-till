﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ASTQueueSystem : MonoBehaviour
{
    public static ASTQueueSystem Instance;

    [Header("AST Waiting Queue List")]
    public List<GameObject> ASTWaitingAreaList = new List<GameObject>();
    public float ValueForCustomerToWaitTheOtherCustomerToMove = 0.4f;
    NavMeshAgent nav;
    public float counter = 5f;
    float counterDefault;
    bool anyoneInQueue = false;

    private void Awake()
    {
        counterDefault = counter;
        Instance = this;
    }

    private void OnTriggerEnter(Collider other)
    {
        // Entering AST Queue area
        //Debug.Log("Enter AST Queue area" + other.name);

        if (other.name != "Support")
        {
            //Debug.Log("AQ Adding " + other.name + " @ position " + ASTWaitingAreaList.Count);
            Customer1 script = other.gameObject.GetComponent<Customer1>();
            Debug.Assert(script.state == State.GoAstQueue);
            script.state = State.InAstQueue;

/*
            if (ASTWaitingAreaList.Count ==2)
            {
                if (other.gameObject != ASTWaitingAreaList[0].gameObject &&
                    other.gameObject != ASTWaitingAreaList[1].gameObject)
                {
                    ASTWaitingAreaList.Add(other.gameObject);
                }
            }
            else if(ASTWaitingAreaList.Count < 2)
            {
                ASTWaitingAreaList.Add(other.gameObject);
            }*/
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.name != "Support")
        {
            if (ASTWaitingAreaList.Count == 1)
            {
                if (ASTWaitingAreaList[0] == null)
                {
                    ASTWaitingAreaList.RemoveAt(0);

                    nav = other.GetComponent<NavMeshAgent>();
                    nav.SetDestination(gameObject.transform.GetChild(0).transform.position);

                    Debug.Log("destoryed");
                }
            }
            else if (ASTWaitingAreaList.Count == 2)
            {
                if (ASTWaitingAreaList[0] == null)
                {
                    ASTWaitingAreaList.RemoveAt(0);

                    nav = other.GetComponent<NavMeshAgent>();
                    nav.SetDestination(gameObject.transform.GetChild(0).transform.position);

                    Debug.Log("destoryed");
                }
                if (ASTWaitingAreaList[1] == null)
                {
                    ASTWaitingAreaList.RemoveAt(1);

                    nav = other.GetComponent<NavMeshAgent>();
                    nav.SetDestination(gameObject.transform.GetChild(0).transform.position);
                }
            }

            if (ASTWaitingAreaList.Count > 0 )
            {
                anyoneInQueue = true;
            }
            else
            {
                anyoneInQueue = false;
            }

            Debug.Log(other.name);

            if (ASTWaitingAreaList.Count > 0)
            {
                nav = ASTWaitingAreaList[0].GetComponent<NavMeshAgent>();

            }

            for (int i = 0; i < ASTWaitingAreaList.Count; i++)
            {
                //send ast1
                if (CM_Handler.Instance.AvailableTills_AST_1.Count == 1 && 
                    CM_Handler.Instance.UnavailableTill_AST_1.Count != 1)
                {
                    if (!nav.name.Contains("/1") && !nav.name.Contains("/2"))
                    {
                        nav.SetDestination(CM_Handler.Instance.AvailableTills_AST_1[0].transform.position);
                        nav.gameObject.name = nav.gameObject.name + "/1";
                    }
                }
                //send ast2
                else if (CM_Handler.Instance.AvailableTills_AST_1.Count == 0 && 
                        CM_Handler.Instance.AvailableTills_AST_2.Count == 1)
                {
                    if (!nav.name.Contains("/2") && !nav.name.Contains("/1"))
                    {
                        nav.SetDestination(CM_Handler.Instance.AvailableTills_AST_2[0].transform.position);
                        nav.gameObject.name = nav.gameObject.name + "/2";
                    }
                }

                //wait in the queue//Defaultcode here
                else if (other.gameObject == ASTWaitingAreaList[i].gameObject)
                {
                    nav = other.GetComponent<NavMeshAgent>();
                    nav.SetDestination(gameObject.transform.GetChild(0).transform.position);
                    Debug.Log("Sent to a place" + "/" + i);
                }
            }
        }
    }
    private void Update()
    {
        if (anyoneInQueue)
        {
            counter -= 1f * Time.deltaTime;
        }
        if (ASTWaitingAreaList.Count < 1)
        {
            anyoneInQueue = false;
            counter = counterDefault;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.name != "Support")
        {
            other.GetComponentInChildren<SpriteRenderer>().sortingLayerName = "6";
            StartCoroutine(WaitBeforeRemoving(other));
        }
    }
    IEnumerator WaitBeforeRemoving(Collider other)
    {
        yield return new WaitForSeconds(.2f);

        string[] divded = other.name.Split('/');

        if (divded[1] == "1")
        {
            ExitTheTrigger1(1);
        }
        else if (divded[1] == "2")
        {
            ExitTheTrigger1(2);
        }

        other.GetComponent<NavMeshAgent>().stoppingDistance = 2f;

        ASTWaitingAreaList.Remove(other.gameObject);
    }
    public void ExitTheTrigger1(int whichTill)
    {
        if (whichTill == 1)
        {
            if (CM_Handler.Instance.UnavailableTill_AST_1.Count == 0)
            {
                CM_Handler.Instance.UnavailableTill_AST_1.Add(CM_Handler.Instance.AvailableTills_AST_1[0]);
                CM_Handler.Instance.AvailableTills_AST_1.RemoveAt(0);
            }
        }
        else if (whichTill ==2 )
        {
            if (CM_Handler.Instance.UnavailableTill_AST_2.Count == 0)
            {
                CM_Handler.Instance.UnavailableTill_AST_2.Add(CM_Handler.Instance.AvailableTills_AST_2[0]);
                CM_Handler.Instance.AvailableTills_AST_2.RemoveAt(0);
            }
        }
    }
}


