﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class KioskQueueSystem : MonoBehaviour {

    public static KioskQueueSystem Instance;

    [Header("Kiosk Waiting Queue List")]
    public List<GameObject> KioskWaitingQueueList = new List<GameObject>();
    public float ValueForCustomerToWaitTheOtherCustomerToMove = 0.4f;
    NavMeshAgent nav;
    public List<GameObject> AnimationReset = new List<GameObject>();

    private void Awake()
    {
        Instance = this;
    }

    private void OnTriggerEnter(Collider other)
    {
        // Entering Kiosk Queue area
        //Debug.Log("Enter Kiosk Queue area" + other.name);
        if (other.name != "Support")
        {
            other.GetComponentInChildren<SpriteRenderer>().sortingLayerName = "4";
            //Debug.Log("KQ Adding " + other.name + " @ position " + KioskWaitingQueueList.Count);
            Customer1 customer1 = other.gameObject.GetComponent<Customer1>();
            Debug.AssertFormat(customer1.state == State.GoKioskQueue, "Enter kiosk queue with state " + customer1.state + ", name " + customer1.customerName);
            if (customer1.state == State.InFirstQueue)
            {
                // This does happen
                Debug.Log(customer1.customerName + " entered kiosk queue - send them back!");
                CM_Handler.Instance.SendBackToFirstQueue(other.gameObject);
                return;
            }
            customer1.state = State.InKioskQueue;
            KioskWaitingQueueList.Add(other.gameObject);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        // In kiosk area
        //Debug.Log("OnTriggerStay " + other.name);
        if (other.name != "Support")
        {
            other.GetComponentInChildren<SpriteRenderer>().sortingLayerName = "4";
            nav = KioskWaitingQueueList[0].GetComponent<NavMeshAgent>();

            for (int i = 0; i < KioskWaitingQueueList.Count; i++)
            {
                if (CM_Handler.Instance.AvailableTills_Kiosk.Count != 0 && CM_Handler.Instance.UnavailableTill_Kiosk.Count == 0)
                {
                    if (KioskWaitingQueueList.Count != 0 && CM_Handler.Instance.AvailableTills_Kiosk.Count != 0)
                    {
                        nav.GetComponent<NavMeshAgent>().SetDestination(CM_Handler.Instance.AvailableTills_Kiosk[0].transform.position);
                        nav.GetComponentInChildren<SpriteRenderer>().sortingLayerName = "4";
                    }
                }
                else if (nav.transform.position != gameObject.transform.GetChild(0).transform.position)
                {
                    nav.SetDestination(gameObject.transform.GetChild(0).transform.position);
                    nav.GetComponentInChildren<SpriteRenderer>().sortingLayerName = "4";
                }               
            }
            if (KioskWaitingQueueList.Count > 1)
            {
                if (KioskWaitingQueueList[1].transform.position != gameObject.transform.GetChild(1).transform.position)
                {
                    KioskWaitingQueueList[1].GetComponentInChildren<SpriteRenderer>().sortingLayerName = "5";
                    KioskWaitingQueueList[1].GetComponent<NavMeshAgent>().SetDestination(gameObject.transform.GetChild(1).transform.position);
                }
            }
            else if (KioskWaitingQueueList.Count == 1)
            {
                KioskWaitingQueueList[0].GetComponentInChildren<SpriteRenderer>().sortingLayerName = "4";
            }
            //First colleauge movement if there is a person in kiosk and they are not special customer then move there without waiting for a click
            if (KioskWaitingQueueList.Count > 0)
            {
                if (CM_Handler.Instance.CoopFirstEmployee.GetComponent<Animator>().enabled == false)
                {
                    if (!KioskWaitingQueueList[0].GetComponent<Customer1>().SecondColleauge)
                    {
                        CM_Handler.Instance.CoopFirstEmployee.GetComponent<Animator>().enabled = true;
                    }
                }
            }
        }  
    }
  
    private void OnTriggerExit(Collider other)
    {
        //Debug.Log("Exit Kiosk Queue " + other.name);
        if (other.name != "Support")
        {
            KioskWaitingQueueList.RemoveAt(0);
        }
    }
}
