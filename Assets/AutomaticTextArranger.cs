﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AutomaticTextArranger : MonoBehaviour {

    [Header("Question Texts")]
    public GameObject TextA, TextB, TextC;
    int TextALength;
    int TextBLength;
    int TextCLength;
    // Use this for initialization
    void Start ()
    {
        TextALength = TextA.GetComponent<Text>().text.Length;
        TextBLength = TextB.GetComponent<Text>().text.Length;
        TextCLength = TextC.GetComponent<Text>().text.Length;
    }

    // Update is called once per frame
    void Update ()
    {
        if (TextALength > TextBLength && TextALength > TextCLength)
        {
            if (TextALength > 140)
            { //16
                TextB.GetComponent<Text>().fontSize =
                TextC.GetComponent<Text>().fontSize = 
                TextA.GetComponent<Text>().fontSize = 16;
            }
            else if (TextALength <= 140 && TextALength > 80)
            {//21 
                TextB.GetComponent<Text>().fontSize =
               TextC.GetComponent<Text>().fontSize =
               TextA.GetComponent<Text>().fontSize = 21;
            }
            else if (TextALength <= 80 && TextALength > 50)
            {//28 
                TextB.GetComponent<Text>().fontSize =
               TextC.GetComponent<Text>().fontSize =
               TextA.GetComponent<Text>().fontSize = 28;
            }

           
           
        }
        else if (TextBLength > TextALength && TextBLength > TextCLength)
        {
            if (TextBLength > 140)
            { //16
                TextB.GetComponent<Text>().fontSize =
                TextC.GetComponent<Text>().fontSize =
                TextA.GetComponent<Text>().fontSize = 16;
            }
            else if (TextBLength <= 140 && TextBLength > 80)
            {//21 
                TextB.GetComponent<Text>().fontSize =
               TextC.GetComponent<Text>().fontSize =
               TextA.GetComponent<Text>().fontSize = 21;
            }
            else if (TextBLength <= 80 && TextBLength > 50)
            {//28 
                TextB.GetComponent<Text>().fontSize =
               TextC.GetComponent<Text>().fontSize =
               TextA.GetComponent<Text>().fontSize = 28;
            }

        }
        else if (TextCLength > TextALength && TextCLength > TextBLength)
        {
            if (TextCLength > 140)
            { //16
                TextB.GetComponent<Text>().fontSize =
                TextC.GetComponent<Text>().fontSize =
                TextA.GetComponent<Text>().fontSize = 16;
            }
            else if (TextCLength <= 140 && TextCLength > 80)
            {//21 
                TextB.GetComponent<Text>().fontSize =
               TextC.GetComponent<Text>().fontSize =
               TextA.GetComponent<Text>().fontSize = 21;
            }
            else if (TextCLength <= 80 && TextCLength > 50)
            {//28 
                TextB.GetComponent<Text>().fontSize =
               TextC.GetComponent<Text>().fontSize =
               TextA.GetComponent<Text>().fontSize = 28;
            }
        }
    }
}
