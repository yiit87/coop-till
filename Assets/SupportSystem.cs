﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

[RequireComponent(typeof(BoxCollider))]
public class SupportSystem : MonoBehaviour {
    public static SupportSystem Instance;

    [Header("Support Button Image")]
    public GameObject SupportButton;
    public float supportButtonTime;
   // public float SupportSpeedMultiplierValue;
  //  public float SupTillDivideTheWaitTimeBy;
    public GameObject SupGOParent;
    private float valueTime = 1f;
    private float SupButtonImageFillAmount = 0f;

    private Image buttonImage;
    GameObject sup;

    [Header("Bonus Speed Value: This is added to total speed of the customer")]
    public float WalkingSpeedWithBonus;
    public float TillSpeedWithBonus;

    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        sup = SupGOParent.transform.GetChild(0).gameObject;

        buttonImage = SupportButton.GetComponent<Image>();
        valueTime = (1 / supportButtonTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (CM_Handler.Instance.support)
        {
            GameObject go = other.gameObject;
            NavMeshAgent nav = go.GetComponent<NavMeshAgent>();
            if (go.GetComponent<Customer1>().customerName != "Support")
            {
                if (go.GetComponent<NavMeshAgent>().speed == 30)
                {
                    nav.speed += WalkingSpeedWithBonus;
                }
                else if (go.GetComponent<NavMeshAgent>().speed == 20)
                {
                    nav.speed += WalkingSpeedWithBonus;
                }
            }
        }
    }

    //TODO: do we want to reset the speed to normal once the support mate leaves the screen ?
    private void OnTriggerExit(Collider other)
    {
        if (!CM_Handler.Instance.support)
        {
            GameObject go = other.gameObject;
            NavMeshAgent nav = go.GetComponent<NavMeshAgent>();
            if (go.GetComponent<Customer1>().customerName != "Support")
            {

                if (go.GetComponent<NavMeshAgent>().speed == 30)
                {
                    //go.GetComponent<Customer1>().speed =
                    nav.speed -= WalkingSpeedWithBonus;
                }
                else if (go.GetComponent<NavMeshAgent>().speed == 20)
                {
                    nav.speed -= WalkingSpeedWithBonus;
                }
            }
        }
    }

    private void Update()
    {
        if (CM_Handler.Instance.waitingQueuePositionList.Count > 1 && //check if there are 2 customers in the queue at least
            buttonImage.fillAmount >=1 &&  //if button image is filled full
            SupGOParent.transform.GetChild(0).localPosition == new Vector3(7.899994f, 7.001202f, 23.29999f)  //if support guy is in the correct location
            //!CM_Handler.Instance.SecondColleagueInUse
            ) //if the second colleague is not appeared
        {
           if(!CM_Handler.Instance.SecondColleague.transform.GetChild(0).GetComponent<SpriteRenderer>().enabled)
            {
                SupportButton.GetComponent<Button>().interactable = true;
            }
        }
        else
        {
            SupportButton.GetComponent<Button>().interactable = false;
        }

        //if support button pressed
        if (CM_Handler.Instance.support)
        {
            SetTheDestinationForSupportGuy(1);

            if (buttonImage.fillAmount <= 1)
            {
                SupButtonImageFillAmount += valueTime * Time.deltaTime;
                buttonImage.fillAmount = SupButtonImageFillAmount;
            }

            if (SupButtonImageFillAmount >= 1)
            {
                SupButtonImageFillAmount = 0f;
                SetTheDestinationForSupportGuy(0);
                CM_Handler.Instance.support = false;
            }
        }
    }

    public void SetTheDestinationForSupportGuy(int value)
    {
        NavMeshAgent nav = sup.GetComponent<NavMeshAgent>();
        nav.SetDestination(CM_Handler.Instance.supportTargetLocation[value].transform.position);
    }
}
