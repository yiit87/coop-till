﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Login1 : MonoBehaviour {
    public GameObject userID;
    Text id;

    public static Login1 Instance;
    public Text loginText;
    public Text ErrorText;
    public Image LoginErrorImage;
    public GameObject InternetErrorImage;
    public GameObject LoadingBar;
    public Slider slider;

    public InputField input;
    public Button SubButton;

   // public CanvasGroup introGroup;

    private AsyncOperation async;
    private bool fadeIn;
    private bool fadeOut;
    //private bool fadeIntro;
    private bool loginSuccessful;
    private bool loading;
    private float alpha;
 //   private AudioSource Audio;

    private void Awake()
    {
        Instance = this;
        LoginErrorImage.gameObject.SetActive(false);

    }

    // Use this for initialization
    void Start()
    {
      //  Audio = GetComponent<AudioSource>();
        alpha = 0;
    }

    // Update is called once per frame
    void Update()
    {

        //Debug.Log("Internet reachability = " + Application.internetReachability);
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            InternetErrorImage.SetActive(true);
            SubButton.interactable = false;
            input.interactable = false;
        }
        else if (loading == false)
        {
            InternetErrorImage.SetActive(false);
            SubButton.interactable = true;
            input.interactable = true;
        }

        if (InternetSubmission.Instance.transmitting)
        {
            SubButton.interactable = false;
            input.interactable = false;
        }
        else if (loading == false)
        {
            SubButton.interactable = true;
            input.interactable = true;
        }

        if (Input.GetKeyDown(KeyCode.Return))
        {
            Submit();
        }

        if (LoginErrorImage.color.a < 1 && fadeIn)
        {
            LoginErrorImage.color = new Color(1, 1, 1, alpha += (Time.deltaTime * 5));
        }
        else if (LoginErrorImage.color.a >= 1)
        {
            StartCoroutine("Timer");
            fadeIn = false;
        }

      /*  if (fadeIntro && introGroup.alpha > 0)
        {
            introGroup.alpha -= Time.deltaTime;
        }*/

        if (LoginErrorImage.color.a > 0 && fadeOut)
        {
            LoginErrorImage.color = new Color(1, 1, 1, alpha -= (Time.deltaTime / 2));
        }
        else if (LoginErrorImage.color.a <= 0)
        {
            fadeOut = false;
        }
    }

    public void Submit()
    {
        Debug.Log("here");
        if (loginText.text.Length < 6)
        {
            LoginErrorImage.gameObject.SetActive(true);

            fadeIn = true;
           /* if (!Audio.isPlaying)
            {
                Audio.Play();
            }*/
            return;
        }
        else
        {
            PlayerPrefs.SetString("UserID", loginText.text);
            StartCoroutine("LoginProcess");
        }
    }

    public void WasSuccessful(bool state)
    {
        loginSuccessful = state;
    }

/*    public void FadeIntro()
    {
        fadeIntro = true;
    }*/
    public void GuestLogin()
    {
        if (InternetSubmission.Instance.HasInternet())
        {
            InternetSubmission.Instance.StartCoroutine("Login", "123456");

            GlobalData.getInstance().StartTest(loginText.text);
            //Debug.Log("Current Employee = " + GlobalData.getInstance().currentEmployee);
            //Debug.Log("SessionStartTime = " + GlobalData.getInstance().SessionStartTime);
            input.interactable = false;
            SubButton.interactable = false;
            StartCoroutine("LoadLevel");
        }
        else
        {
            //Debug.Log("No internet");
        }
    }
    IEnumerator LoginProcess()
    {
        if (InternetSubmission.Instance.HasInternet())
        {
            Coroutine c = InternetSubmission.Instance.StartCoroutine("Login", loginText.text);
            yield return c;

            if (!loginSuccessful)
            {
                //display("Hub ID not found");
                fadeIn = true;
               /* if (!Audio.isPlaying)
                {
                    Audio.Play();
                }*/
                yield break;
            }

            GlobalData.getInstance().StartTest(loginText.text);
            //Debug.Log("Current Employee = " + GlobalData.getInstance().currentEmployee);
            //Debug.Log("SessionStartTime = " + GlobalData.getInstance().SessionStartTime);
            input.interactable = false;
            SubButton.interactable = false;
            StartCoroutine("LoadLevel");
        }
        else
        {
            //Debug.Log("No internet");
        }
    }

    IEnumerator Timer()
    {
        yield return new WaitForSeconds(0.5f);
        fadeOut = true;
    }

    IEnumerator LoadLevel()
    {
        loading = true;
        LoadingBar.SetActive(true);
        SubButton.interactable = false;
        input.interactable = false;
        async = SceneManager.LoadSceneAsync(1);
        async.allowSceneActivation = false;

        while (async.isDone == false)
        {
            slider.value = async.progress;
            if (async.progress == 0.9f)
            {
                slider.value = 1f;
                async.allowSceneActivation = true;
            }
            yield return null;
        }
    }
}
